<?php

require_once 'src/php/conf.php';

session_start();

$user_info = mysqli_fetch_array($mysqli->query('SELECT * FROM users WHERE id = '.$_SESSION['user_id']));
			
$_SESSION['role'] = $user_info['role'];

// Hiljem liigutab selle eraldi faili!

if ( $_SESSION['role'] == 1 ) { // registered user

	$_SESSION['permissions'] = array(0,0,0,0,0,1,0);

	/*$show_users = 0;
	$show_trainings = 0;
	$show_trainings2 = 0;
	$show_summercamps = 0;*/
	
	$role = "Registreeritud kasutaja";


} else if ( $_SESSION['role'] == 2 ) { // lecturer - koolitaja

	$_SESSION['permissions'] = array(1,1,0,1,0,1,1);
	
	$role = "Koolitaja";

} else if ( $_SESSION['role'] == 3 ) { // supervisor - juhendaja

	$_SESSION['permissions'] = array(1,0,1,1,0,1,1);
	
	$role = "Juhendaja";


} else if ( $_SESSION['role'] == 4 ) { // member (laps)

	$_SESSION['permissions'] = array(0,0,0,0,0,1,0);
	$role = "Liige (laps)";


} else if ( $_SESSION['role'] == 5 ) { // admin

	$_SESSION['permissions'] = array(1,1,1,1,1,1,1);
	$role = "Administraator";


} else {

	$_SESSION['permissions'] = array(0,0,0,0,0,0,0);
	$role = "Registreeritud kasutaja";


}

/* input_get annab vastava lehe URL parameetrid massiivina 
view määrab ära selle osa, mida tahame sellest massiivist kasutada*/
$view = filter_input(INPUT_GET, 'view', FILTER_SANITIZE_STRING);

// Kui kasutaja on sattunud antud lehele sisse-logimata, suuname ta tagasi avalehele
//isset - Determine if a variable is set and is not NULL
if (!isset($_SESSION['username'])) {
	if ( isset($view) && $view != "") {
		header ( 'Location: index.php?redirect=' . $view );	
	} else {	
		header ( 'Location: index.php' );
	}
}

//määratakse content muutuja väärtus
$content = 'src/php/contents/home.php';

//muudetakse antud tingumuse täitumisel content muutuja väärtust
if ( isset($view) && $view != "") {
	$content = 'src/php/contents/'. $view . '.php';
}

?>
<!DOCTYPE html>
<html lang=ee>
<head>
<meta charset=ISO-8859-1>
<meta http-equiv=X-UA-Compatible content="IE=edge">
<meta name=viewport content="width=device-width, initial-scale=1">
<meta name=description content>
<meta name=author content="Aivo Toots, Kai Sellin, Umar Zarip">
<title>TORE Maailmavaade</title>
<link href=src/css/bootstrap.min.css rel=stylesheet>
<link href=src/css/font-awesome.min.css rel=stylesheet>
<link href=src/css/certificate.css rel=stylesheet>
<script src=https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js></script>
<script src=http://getbootstrap.com/dist/js/bootstrap.min.js></script>
<script src=src/js/tore.js></script>
<script src=src/js/jspdf.min.js></script>
<?php if ($view == "profile") { ?>
<script src=src/js/jquery.inlineedit.js></script>
<script src=src/js/profile.js></script>
<?php } ?>
</head>
<body>
<?php
    	include('src/php/navigation.php');
 		include($content);
 	?>
</body>
</html>