<?php

require_once 'conf.php';

session_start();

switch ( $_GET['action'] ) {

	case "logIN":
		
		if ($mysqli->connect_errno) {
    
		    echo "Failed to connect to MySQL: " . $mysqli->connect_errno;
		
		}
		
		$redirect = filter_input(INPUT_GET, 'redirect', FILTER_SANITIZE_STRING);
		$username = filter_input(INPUT_POST, 'inputEmail', FILTER_SANITIZE_STRING);
		$password = filter_input(INPUT_POST, 'inputPassword', FILTER_SANITIZE_STRING);
		$password_md5 = md5 ( $password );
		
		// Kontrollime andmebaasist, kas sisestatud andmetega kasutaja ekisteerib
		$query = 'SELECT * FROM users WHERE username = "'.$username.'" AND password = "'.$password_md5.'"';
		$result = $mysqli->query($query);
		
		if ( $result->num_rows == 1 ) {
		
			// Leiame sisestatud andmetega kasutaja kohta veel andmeid ja salvestame need sessiooni
			$query = 'SELECT * FROM users WHERE username = "'.$username.'" AND password = "'.$password_md5.'"';
			$result = $mysqli->query($query);
			$row = $result->fetch_row();
			
			$_SESSION['username'] = $row[1];
			$_SESSION['user_id'] = $row[0];
			//$_SESSION['role'] = $row[10];
			
			if ( isset($redirect) && $redirect != "") {
				
				header ( 'Location: ../../main.php?view='.$redirect );
				
			} else {
			
				header ( 'Location: ../../main.php' );
			
			}			

		} else {

			print "Vale kasutajanimi ja/vĆµi parool!";
			print '<p><a href="javascript:history.back(0)">&lt;- Tagasi</a></p>';

		}

	break;
	
	case "FB_logIN":

		require_once 'facebook.php';
		
		// Loome �henduse Facebookiga
		$appid 		= APP_ID;
		$appsecret  = APP_SECRET;
		$facebook   = new Facebook(array(
				'appId' => $appid,
				'secret' => $appsecret,
				'cookie' => TRUE,
		));
	
		$fbuser = $facebook->getUser();
	
		if ($fbuser) {
	
			try {
	
				$user_profile = $facebook->api('/me');
	
			} catch (Exception $e) {
					
				echo $e->getMessage();
				exit();
					
			}
			
			$id = 0;
				
			// Kontrollime, kas sisseloginud kasutaja andmed on salvestatud andmebaasi. Kui ei, salvestame need
			$query = 'SELECT * FROM users WHERE username = "'.$user_profile["email"].'"';
			$result = $mysqli->query($query);
			$user_info = mysqli_fetch_array($result);
	
			if($result->num_rows < 1) {
				$name_array = split(" ", $user_profile["name"]);
				$query = 'INSERT INTO users SET username="'.$user_profile["email"].'", firstname="'.$name_array[0].'", lastname="'.$name_array[1].'", gender="'.$user_profile["gender"].'", registered=now(), last_login=now(), active=1, role=1';
				$results = $mysqli->query($query);
				$id = $mysqli->insert_id;	
			
			} else {			
				$id = $user_info['id'];
			}			
	
			$_SESSION['username'] = $user_profile["email"];
			//$_SESSION['role'] = $user_info["role"];
			$_SESSION['user_id'] = $id;
			
			$redirect = filter_input(INPUT_GET, 'redirect', FILTER_SANITIZE_STRING);
			
			if ( isset($redirect) && $redirect != "") {
				
				header ( 'Location: ../../main.php?view='.$redirect );
				
			} else {
			
				header ( 'Location: ../../main.php' );
			
			}	
						
		}
		
		break;

	case "logOUT":

		session_destroy();
			
		header ( 'Location: ../../index.php' );

	break;

	default:
	
		header ( 'Location: ../../index.php' );

}
?>