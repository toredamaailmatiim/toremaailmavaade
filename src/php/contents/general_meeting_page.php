<div class="container">
	<div class="row">&nbsp;</div>
	<div class="row">&nbsp;</div>
	<div class="row">&nbsp;</div>
	<div class="row">&nbsp;</div>
	<div class="row">
		<div class="col-md-9">
			<div class="panel panel-default">
				<!-- Default panel contents -->
				<div class="panel-heading"><h2>Uldkoosolek</h2>
				</div>
				<div class="panel-body">
					<form class="navbar-form navbar-left" role="search" method="POST"
						action="main.php?view=general_meeting">
						<div class="form-group">
							<input type="text" name="searchValue" class="form-control"
								placeholder="Otsi uldkoosolekut">
						</div>
						<button type="submit" class="btn btn-default">Otsi</button>
					</form>
				</div>

				<!-- Table -->
				<div class="table-responsive">
				<table class="table table-hover">
					<thead>
						<tr>
							<th>#</th>
							<th>Nimi</th>
							<th>Toimumise aeg</th>
							<th>Asukoht</th>
							<th>Osalejaid</th>
                                                        <th>Kirjeldus</th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php
						// count näitab massiivi pikkust, suurust
						for($i = 0; $i < count ( $array ); $i ++) {
							/* . kasutatakse stringide kokku liitmiseks, nagu javas + kahe stringi vahel
							 siin toimub stringi echomine
							tore.js jooksutatakse juba main.php-s läbi, seega saan seda juba siin kasutada*/
							echo '
								<tr>
									<th scope="row">' . $array [$i] ['id'] . '</th>
									<td>' . $array [$i] ['name'] . '</td>
									<td>' . $array [$i] ['general_meeting_date'] . '</td>
									<td>' . $array [$i] ['location'] . '</td>
									<td><a id="participants-'.$array [$i] ['id'].'" href="#" onclick="General_MeetingJs.viewParticipants('.$array[$i]['id'].');">' . $array [$i] ['participants'] . '</a></td>
                                                                        <td>' . $array [$i] ['description'] . '</td>
									<td><button type="button" class="btn btn-info btn-sm" onclick="General_MeetingJs.openModalForm('.$array[$i]['id'].')">Muuda andmeid</button></td>
									<td><button type="button" class="btn btn-info btn-sm" onclick="General_MeetingJs.remove('.$array[$i]['id'].');">Kustuta koolitus</button></td>

									<td></td>
								</tr>';
						}
						?>	
                                            
					</tbody>
				</table>
				</div>
			</div>
			<p>
				<a class="btn btn-lg btn-primary" data-toggle="modal" href="#addnewMainGeneralMeetingModal">Lisa uus
					Uldkoosolek</a>
			</p>
		</div>

	</div>

</div>
<!-- /container -->

<div class="modal fade" id="addnewMainGeneralMeetingModal" tabindex="-1"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span>
					<span class="sr-only">X</span>
				</button>
				<h3 class="modal-title" id="myModalLabel">Registreeri uus
					Üldkoosolek:</h3>
			</div>
			<div class="modal-body">

				<form method="POST"
					action="src/php/contents/general_meeting.php?action=add_general_meeting">

					<div class="form-horizontal">
						<input type="hidden" class="form-control" name="id" />
						<div class="form-group">
							<label class="col-md-5 control-label">Nimi:</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="name" required />
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-5 control-label">Kuupäev:</label>
							<div class="col-md-4">
								<input type="date" class="form-control" name="camp_begin" required />
							</div>
                                                        </div>
                                                    
						<div class="form-group">
							<label class="col-md-5 control-label">Asukoht:</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="location" required />
							</div>
                                                        </div>
                                                        
						<div class="form-group">
							<label class="col-md-5 control-label">Kirjeldus:</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="description" required />
							</div>
						</div>
						<div class="form-group">
							<input type="submit" class="btn btn-success" name="save_button"
								value="Salvesta">
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Sulge</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="viewParticipants" tabindex="-1"
	role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span>
					<span class="sr-only">X</span>
				</button>
				<h3 class="modal-title" id="myModalLabel2">Osalejad:</h3>
			</div>
			<div class="modal-body">
				<table class="table table-hover">
					<thead>
						<tr>
							<th>#</th>
							<th>Nimi</th>
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody id="usersTable">
					</tbody>
					<tfoot>
						<tr>
							<td>
								<input type="hidden" class="form-control" name="general_meetingId" />
							</td>
							<td>	
								<select id="userSelect" class="form-control" name="userId" required>
									<option value="">--- Vali kasutaja ---</option>
								</select>
							</td>
							<td>
								<input type="button" class="btn btn-success" name="save_button"
								value="Lisa osaleja" onclick="General_MeetingJs.addParticipant();">
							</td>
						</tr>
					</tfoot>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Sulge</button>
			</div>
		</div>
	</div>
</div>