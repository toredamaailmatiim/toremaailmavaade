    <div class="container-fluid">
        <div class="row">&nbsp;</div>
        <div class="row">&nbsp;</div>
        <div class="row">&nbsp;</div>
        <div class="row">&nbsp;</div>
        <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <!-- Default panel contents -->
                <div class="panel-heading"><h2>Muuda oma andmeid</h2>Vajutage v�ljale, mida soovite muuta</div>
                <div class="panel-body">
                <!-- Table -->
                <div class="table-responsive">
                <table class="table table-hover">
                  <thead>
        <tr>
          <th>#</th>
          <th>Roll</th>
          <th>Sugu</th>
          <th>Eesnimi</th>
          <th>Perekonnanimi</th>
          <th>E-mail</th>
          <th>Isikukood</th>
          <th>Elukoha aadress</th>
          <th>Telefon</th>
          <th>Kool</th>
          <th>Juhendaja nimi</th>
          <th>Vanema nimi</th>
          <th>Vanema e-mail</th>  
          <th>Vanema telefon</th>      
        </tr>
      </thead>
      <tbody>
		<?php
               
		for($i = 0; $i < count ( $array1 ); $i ++) {
                    
			/* . kasutatakse stringide kokku liitmiseks, nagu javas + kahe stringi vahel
                         siin toimub stringi echomine
			tore.js jooksutatakse juba main.php-s läbi, seega saan seda juba siin kasutada*/
				echo '
					<tr>
						<th scope="row">' . $array1 [$i] ['id'] . '</th>
                        <td>' . $array1 [$i] ['role'] . '</td>
						<td>' . $array1 [$i] ['gender'] . '</td>
						<td><span class="editable" id="firstname">' . $array1 [$i] ['firstname'] . '</span></td>
                        <td><span class="editable" id="lastname">' . $array1 [$i] ['lastname'] . '</span></td>
						<td>' . $array1 [$i] ['username'] . '</td>
						<td><span class="editable" id="ss_number">' . $array1 [$i] ['ss_number'] . '</span></td>
                        <td><span class="editable" id="address">' . $array1 [$i] ['address'] . '</span></td>
						<td><span class="editable" id="phone">' . $array1 [$i] ['phone'] . '</span></td>
                        <td><span class="editable" id="school">' . $array1 [$i] ['school'] . '</span></td>
						<td><span class="editable" id="supervisor_name">' . $array1 [$i] ['supervisor_name'] . '</span></td>
						<td><span class="editable" id="parent_name">' . $array1 [$i] ['parent_name'] . '</span></td>
                        <td><span class="editable" id="parent_email">' . $array1 [$i] ['parent_email'] . '</span></td>
                        <td><span class="editable" id="parent_phone">' . $array1 [$i] ['parent_phone'] . '</span></td>
					</tr>';
			}
			?>
      </tbody>
                </table>
                </div>
                </div>
                <div class="panel-footer"><p></p></div>
              </div>
        </div>
            
            <div class="col-md-12"></div>
            
        </div>

    </div> <!-- /container -->