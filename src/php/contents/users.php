<?php

session_start();

if ($_SESSION['permissions'][0] == 1) {

	if(isset($_GET['action'])) {
		
		switch ( $_GET['action'] ) {
			
			case "getTrainingUsers":
				
				require_once '../conf.php';
				
				$trainingId  = $mysqli->real_escape_string($_POST['trainingId']);
				
				$result = $mysqli->query('SELECT u.* from users u join trainings_participants t on t.users_id = u.id where t.trainings_id = '.$trainingId);
				$registered_users = array();
				while($user_info = mysqli_fetch_array($result)) {
					$registered_users[$user_info['id']] = $user_info;
				}
				
				
				$result = $mysqli->query('SELECT * from users');
				$not_registered_users = array();
				while($user_info = mysqli_fetch_array($result)) {
					if(!isset($registered_users[$user_info['id']])) {
						$not_registered_users[] = $user_info;
					}
				}
				
				$users = array(
					'not_registered' => $not_registered_users,
					'registered' => $registered_users
				);
				
				header('Content-Type: application/json');
				echo json_encode($users);
				break;
				
			case "getTraining2Users":
					
				require_once '../conf.php';
					
				$trainingId  = $mysqli->real_escape_string($_POST['trainingId']);
					
				$result = $mysqli->query('SELECT u.* from users u join trainings2_participants t on t.users_id = u.id where t.trainings2_id = '.$trainingId);
				$registered_users = array();
				while($user_info = mysqli_fetch_array($result)) {
					$registered_users[$user_info['id']] = $user_info;
				}
					
					
				$result = $mysqli->query('SELECT * from users');
				$not_registered_users = array();
				while($user_info = mysqli_fetch_array($result)) {
					if(!isset($registered_users[$user_info['id']])) {
						$not_registered_users[] = $user_info;
					}
				}
					
				$users = array(
						'not_registered' => $not_registered_users,
						'registered' => $registered_users
				);
					
				header('Content-Type: application/json');
				echo json_encode($users);
				break;
			
			default: 
				break;
	
		
	        
	                case "getSummercampUsers":
				
				require_once '../conf.php';
				
				$summercampId  = $mysqli->real_escape_string($_POST['summercampId']);
				
				$result = $mysqli->query('SELECT u.* from users u join summercamp_participants scp on scp.users_id = u.id where scp.summercamp_id = '.$summercampId);
				$registered_users = array();
				while($user_info = mysqli_fetch_array($result)) {
					$registered_users[$user_info['id']] = $user_info;
				}
				
				
				$result = $mysqli->query('SELECT * from users');
				$not_registered_users = array();
				while($user_info = mysqli_fetch_array($result)) {
					if(!isset($registered_users[$user_info['id']])) {
						$not_registered_users[] = $user_info;
					}
				}
				
				$users = array(
					'not_registered' => $not_registered_users,
					'registered' => $registered_users
				);
				
				header('Content-Type: application/json');
				echo json_encode($users);
				break;
			
			default:
				header ( 'Location: ../../../main.php?view=users' );
	        }
		
	} else {
	
		if ($mysqli->connect_errno) {
			echo "Failed to connect to MySQL: " . $mysqli->connect_errno;
		}
			
		$searchValue = filter_input(INPUT_POST, 'searchValue', FILTER_SANITIZE_STRING);
			
		$result = $mysqli->query('SELECT users.id, users.username, 
	                 users.ss_number, users.firstname, 
	                 users.lastname, users.gender, 
	                 users.address, users.phone, users.role, 
	                 users.school, members.supervisor_name, 
	                 members.parent_name, members.parent_email, 
	                 members.parent_phone FROM users LEFT JOIN members 
	                 ON members.user_id=users.id ORDER BY users.id');
		$array1 = array();
		while($row = mysqli_fetch_array($result)) {
			$array1[] = $row;
		}
		
		include('users_list.php');
			
	}

} else {

	header('Location: main.php?view=home');

}

?>