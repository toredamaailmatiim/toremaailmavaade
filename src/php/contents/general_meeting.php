<?php

if(isset($_GET['action'])) {
	switch ( $_GET['action'] ) {
	
		// case on nagu tegusõna
		case "add_general_meeting":
	
			if ($mysqli->connect_errno) {
	
				echo "Failed to connect to MySQL: " . $mysqli->connect_errno;
	
			}
			
			require_once '../conf.php';
	
			$name  = $mysqli->real_escape_string($_POST['name']);
			$general_meeting_date  = $mysqli->real_escape_string($_POST['camp_begin']);
			$location  = $mysqli->real_escape_string($_POST['location']);
                        $description  = $mysqli->real_escape_string($_POST['description']);
			$idregistrar  = $mysqli->real_escape_string($_SESSION['user_id']);
	
			/* echo aitab printida ja exit tapab koodi
			. on nagu + on Javas (Stringide liitmiseks)*/
			if($id == 0) {
				$query = "INSERT INTO general_meeting VALUES (0, '".$name."', '".$general_meeting_date."','".$location."', '".$idregistrar."','".$description."')";
				$mysqli->query($query) or die($query.'<br />'.$mysqli->error);
			} else {
				$query = "UPDATE general_meeting set name = '".$name."', general_meeting_date = '".$general_meeting_date."', location = '".$location."',description = '".$description."' WHERE id = '".$id."'";
				$mysqli->query($query) or die($query.'<br />'.$mysqli->error);
			}
	
			header ( 'Location: ../../../main.php?view=general_meeting' );	
		
		case "findById":
			require_once '../conf.php';
			// väärtustame muutuja id tore.js-st postitud
			$id = $mysqli->real_escape_string($_POST['id']);
			
			$query = 'SELECT * FROM general_meeting WHERE id = "'.$id.'"';
			$result = $mysqli->query($query);
			$row = $result->fetch_row();
			// määrab, mis tüüpi sisu edastatakse
			header('Content-Type: application/json');
			echo json_encode($row);
			break;
		
			
		case "addParticipant":
			require_once '../conf.php';
			
			$userId = $mysqli->real_escape_string($_POST['userId']);
			$general_meetingId = $mysqli->real_escape_string($_POST['general_meetingId']);
			if($userId == 0){
                         break   ;
                        }
			$query = "INSERT INTO general_meeting_participants VALUES ('".$general_meetingId."', '".$userId."')";
			$mysqli->query($query) or die($query.'<br />'.$mysqli->error);
			break;
			
		case "removeParticipant":
			require_once '../conf.php';
				
			$userId = $mysqli->real_escape_string($_POST['userId']);
			$general_meetingId = $mysqli->real_escape_string($_POST['general_meetingId']);
				
			$query = "DELETE FROM general_meeting_participants WHERE general_meeting_Id = ".$general_meetingId." and users_id = ".$userId."";
			$mysqli->query($query) or die($query.'<br />'.$mysqli->error);
			break;
			
		case "deleteById":
			require_once '../conf.php';

			$id = $mysqli->real_escape_string($_POST['id']);
				
			$query = 'DELETE FROM general_meeting WHERE id = "'.$id.'"';
			$result = $mysqli->query($query);
			break;	
			// defaulti minnakse kui ühegi muu case'i sisse ei minda
		default:
			header ( 'Location: ../../../main.php?view=general_meeting' );
	}
} else {
	if ($mysqli->connect_errno) {
		echo "Failed to connect to MySQL: " . $mysqli->connect_errno;
	}
	
	// andmebaasi objekt, millest mysqli_fetch_array abil tehakse igast reast array
	// kui on näiteks LIKE a%, siis ta otsib kõik a-ga algavad (% tähendab, et sealt edasi on suvaline)
	
	$searchValue = filter_input(INPUT_POST, 'searchValue', FILTER_SANITIZE_STRING);
	
	$result = $mysqli->query('SELECT general_meeting.id, general_meeting.name,
                 general_meeting.general_meeting_date,general_meeting.location,
                 general_meeting.user_id,general_meeting.description , COUNT(general_meeting_participants.users_id)
                 AS participants FROM general_meeting LEFT JOIN general_meeting_participants
                 ON general_meeting.id=general_meeting_participants.general_meeting_id
                 WHERE
                 (name LIKE "%'.$searchValue.'%" OR
                 location LIKE "%'.$searchValue.'%"  OR 
                 general_meeting_date LIKE "%'.$searchValue.'%")  GROUP BY general_meeting.id');
	
        
	$array = array();
	while($row = mysqli_fetch_array($result)) {
		$array[] = $row;
	}
       

	// siit tuleb container'i sisu (mis muidu oli enne main.php-s)
}
$sql_table_creation_general_meeting = "CREATE TABLE IF NOT EXISTS general_meeting (
			id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
			name VARCHAR(50),
			general_meeting_date DATE,
			location VARCHAR(30) NOT NULL,
                        user_id int(11),
			description TEXT CHARACTER SET utf8)";

$sql_table_creation_general_meeting_participants = "CREATE TABLE IF NOT EXISTS general_meeting_participants (
			general_meeting_id int(11),
                        users_id int(11))";

$mysqli->query($sql_table_creation_general_meeting);
$mysqli->query($sql_table_creation_general_meeting_participants);

include('general_meeting_page.php');
?>