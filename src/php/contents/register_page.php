
    <div class="container">
        <div class="row">&nbsp;</div>
	<div class="row">&nbsp;</div>
	<div class="row">&nbsp;</div>
	<div class="row">&nbsp;</div>
	<div class="row">
        <div class="row">
        <div class="col-md-3"></div>
        <form role="form" action="src/php/register.php?action=registerNewUser" method="post">            
            <div class="col-md-6">
                <h2 class="form-signin-heading text-center">Registreeri kasutajaks</h2>
                <div class="form-group">
                    <label for="inputFirstName">Eesnimi</label>
                    <div class="input-group">
                        <input type="text" class="form-control" name="inputFirstName" id="inputFirstName" placeholder="Eesnimi" required>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputLastName">Perekonnanimi</label>
                    <div class="input-group">
                        <input type="text" class="form-control" name="inputLastName" id="inputLastName" placeholder="Perekonnanimi" required>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword1">Parool 1x</label>
                    <div class="input-group">
                        <input type="password" class="form-control" name="inputPassword1" id="inputPassword1" placeholder="Parool 1x" required>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword2">Parool 2x</label>
                    <div class="input-group">
                        <input type="password" class="form-control" name="inputPassword2" id="inputPassword2" placeholder="Parool 2x" required>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputSSNumber">Isikukood</label>
                    <div class="input-group">
                        <input type="text" class="form-control" id="inputSSNumber" name="inputSSNumber" placeholder="Isikukood" required>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputAddress">Elukoha aadress</label>
                    <div class="input-group">
                        <input type="text" class="form-control" id="inputAddress" name="inputAddress" placeholder="Elukoha aadress" required>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail">E-maili aadress</label>
                    <div class="input-group">
                        <input type="email" class="form-control" id="inputEmail" name="inputEmail" placeholder="E-maili aadress" required>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPhone">Telefon</label>
                        <input type="text" class="form-control" id="inputPhone" name="inputPhone" placeholder="Telefon">
                </div>
                <div class="well well-sm"><strong><span class="glyphicon glyphicon-asterisk"></span>&nbsp;Tähistatud väljad on kohustuslikud</strong></div>
                <input type="submit" name="register" id="register" value="Registreeri" class="btn btn-primary btn-block">
                <input type="submit" name="registerFB" id="registerFB" value="Registreeri Facebookiga" class="btn btn-primary btn-block" onclick="FBLogin()">
            </div>
        </form>
        <div class="row">&nbsp;</div>
	<div class="row">&nbsp;</div>
	<div class="row">

    </div>
	</div> <!-- /container -->

  </body>
