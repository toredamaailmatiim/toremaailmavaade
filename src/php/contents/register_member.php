
    <div class="container">
        <div class="row">&nbsp;</div>
	<div class="row">&nbsp;</div>
	<div class="row">&nbsp;</div>
	<div class="row">&nbsp;</div>
	<div class="row">
        <div class="row">
        <div class="col-md-3"></div>
        <form role="form" action="src/php/register.php?action=registerNewMember" method="post">            
            <div class="col-md-6">
                <h2 class="form-signin-heading text-center">Registreeri liikmeks</h2>
                <div class="form-group">
                    <label for="inputSupervisorName">Juhendaja nimi</label>
                    <div class="input-group">
                        <input type="text" class="form-control" name="inputSupervisorName" id="inputSupervisorName" placeholder="Juhendaja nimi" required>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputParentName">Vanema nimi</label>
                    <div class="input-group">
                        <input type="text" class="form-control" name="inputParentName" id="inputParentName" placeholder="Vanema nimi" required>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputParentEmail">Vanema email</label>
                    <div class="input-group">
                        <input type="email" class="form-control" id="inputParentEmail" name="inputParentEmail" placeholder="Vanema email" required>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputSchool">Kool</label>
                    <div class="input-group">
                        <input type="text" class="form-control" id="inputSchool" name="inputSchool" placeholder="kool" required>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputParentPhone">Vanema Telefon</label>
                    <div class="input-group">
                        <input type="text" class="form-control" name="inputParentPhone" id="inputParentPhone" placeholder="Vanema telefon" required>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword">Parool</label>
                    <div class="input-group">
                        <input type="password" class="form-control" name="inputPassword" id="inputPassword" placeholder="Parool" required>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                </div>
                <div class="well well-sm"><strong><span class="glyphicon glyphicon-asterisk"></span>&nbsp;Tähistatud väljad on kohustuslikud</strong></div>
                <input type="submit" name="register" id="register" value="Registreeri" class="btn btn-primary btn-block">
                
            </div>
        </form>
        <div class="row">&nbsp;</div>
	<div class="row">&nbsp;</div>
	<div class="row">

    </div>
	</div> <!-- /container -->

  </body>
