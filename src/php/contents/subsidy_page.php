<div class="container">
	<div class="row">&nbsp;</div>
	<div class="row">&nbsp;</div>
	<div class="row">&nbsp;</div>
	<div class="row">&nbsp;</div>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<!-- Default panel contents -->
				<div class="panel-heading"><h2>Raha taotlused</h2>
				</div>
				<div class="panel-body">
					<form class="navbar-form navbar-left" role="search" method="POST"
						action="main.php?view=subsidy">
						<div class="form-group">
							<input type="text" name="searchValue" class="form-control"
								placeholder="Otsi taotlust">
						</div>
						<button type="submit" class="btn btn-default">Otsi</button>
					</form>
				</div>

				<!-- Table -->
				<div class="table-responsive">
				<table class="table table-hover">
					<thead>
						<tr>
							<th>#</th>
							<th>E-mail</th>
							<th>Mille jaoks</th>
                                                        <th>Koolituse nimi</th>
							<th>Summa</th>
							<th>Informatsioon</th>
                                                        <th>Rahuldatud</th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php
						// count nĆ¤itab massiivi pikkust, suurust
						for($i = 0; $i < count ( $array ); $i ++) {
							echo '
								<tr>
									<th scope="row">' . $array [$i] ['id'] . '</th>
									<td>' . $array [$i] ['email'] . '</td>
									<td>' . $array [$i] ['taotlemisetyyp'] . '</td>
                                                                        <td>' . $array [$i] ['nimi'] . '</td>
									<td>' . $array [$i] ['summa'] . '</td>
									<td>' . $array [$i] ['informatsioon'] . '</td>
                                                                        <td>' . $array [$i] ['rahuldatud'] . '</td>
									<td><button type="button" class="btn btn-info btn-sm" onclick="SubsidyJs.remove('.$array[$i]['id'].');">Kustuta taotlus</button></td>
                                                                        <td><button type="button" class="btn btn-info btn-sm" onclick="SubsidyJs.openModalForm('.$array[$i]['id'].')">Muuda andmeid</button></td>
                                                                        <td><button type="button" class="btn btn-info btn-sm" onclick="SubsidyJs.approve('.$array[$i]['id'].');">Rahulda taotlus</button></td>
                                                                      <td></td>
								</tr>';
						}
						?>	
                                            
					</tbody>
				</table>
				</div>
			</div>
			<p>
				<a class="btn btn-lg btn-primary" data-toggle="modal" href = "#addnewMainSubsidyModal">
				Lisa uus raha taotlus</a>
			</p>
		</div>
		
	</div>

</div>
<!-- /container -->

<div class="modal fade" id="addnewMainSubsidyModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span>
					<span class="sr-only">X</span>
				</button>
				<h3 class="modal-title" id="myModalLabel">Uus raha taotlus:</h3>
			</div>
			<div class="modal-body">

				<form method="POST" action="src/php/contents/subsidy.php?action=subsidy_add">

					<div class="form-horizontal">
						<input type="hidden" class="form-control" name="id" />
						 <div class="form-group">
                    <label class="col-md-5 control-label" for="isikuEmail">Sisesta 
							 oma e-mail</label>
                    <div class="input-group">
                        <input type="text" class="form-control" name="isikuEmail" id="isikuEmail" placeholder="E-mail" required>
                    </div>
                </div>
                <div class="form-group">
                	<label class="col-md-5 control-label" for="taotlemiseTyyp">Mille jaoks 
					raha taoteldakse?</label>
                	<div class="input-group">
                		<select class="form-control" name="taotlemiseTyyp" id="taotlemiseTyyp" required>
                		<option value="">Vali tüüp</option>
				    	<option value="Koolitus">Koolitus</option>
				    	<option value="Laager">Laager</option>
				    	<option value="Suvelaager">Suvelaager</option>
						</select>
					</div>
				</div>
                 <div class="form-group">
				    <label class="col-md-5 control-label" for="nimi">
					 Koolituse/Laagri nimi</label>
				    <div class="input-group">
				        <input type="text" class="form-control" name="nimi" id="nimi" placeholder="Nimi" required>
				    </div>
                </div>
 				<div class="form-group">
				    <label class="col-md-5 control-label" for="Summa">Summa</label>
				    <div class="input-group">
				        <input type="text" class="form-control" name="Summa" id="Summa" placeholder="Summa" required>
				    </div>
                </div>
			<div class="form-group">
				    <label class="col-md-5 control-label" for="Informatsioon">Informatsioon</label>
				    <div class="input-group">
				        <input type="text" class="form-control" name="Informatsioon" id="Informatsioon" placeholder="Informatsioon" required>
				    
                                    </div>
                </div>
                                                
                                                <div class="form-group">
                                                
							<input type="submit" class="btn btn-success" name="save_button" value="Salvesta">
						</div>
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">
				Sulge</button>
			</div>
			</form>
		</div>
	</div>
</div>
</div>
