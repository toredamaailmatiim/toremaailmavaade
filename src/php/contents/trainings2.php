<?php

session_start();

if ($_SESSION['permissions'][2] == 1) {

	if(isset($_GET['action'])) {
		switch ( $_GET['action'] ) {
		
			// case on nagu tegus�na
			case "register":
			
				require_once '../conf.php';
		
				if ($mysqli->connect_errno) {
		
					echo "Failed to connect to MySQL: " . $mysqli->connect_errno;
				}		
		
				$name  = $mysqli->real_escape_string($_POST['name']);
				$start_date  = $mysqli->real_escape_string($_POST['start_date']);
				$end_date  = $mysqli->real_escape_string($_POST['end_date']);
				$lecturer  = $mysqli->real_escape_string($_POST['lecturer']);
				$location  = $mysqli->real_escape_string($_POST['location']);
				$duration  = $mysqli->real_escape_string($_POST['duration']);
				$id  = $mysqli->real_escape_string($_POST['id']);
		
				/* echo aitab printida ja exit tapab koodi
				. on nagu + on Javas (Stringide liitmiseks)*/
				if($id == 0) {
					$query = "INSERT INTO trainings2 VALUES (0, '".$name."', '".$lecturer."', '".$start_date."', '".$end_date."', '".$location."', '".$duration."', '".$_SESSION['user_id']."')";
					$mysqli->query($query) or die($query.'<br />'.$mysqli->error);
				} else {
					$query = "UPDATE trainings2 set name = '".$name."', start_date = '".$start_date."', end_date = '".$end_date."', lecturer = '".$lecturer."', location = '".$location."', duration = '".$duration."' WHERE id = '".$id."'";
					$mysqli->query($query) or die($query.'<br />'.$mysqli->error);
				}
		
				header ( 'Location: ../../../main.php?view=trainings2' );	
			
			case "findById":
				require_once '../conf.php';
				// v��rtustame muutuja id tore.js-st postitud
				$id = $mysqli->real_escape_string($_POST['id']);
				
				$query = 'SELECT * FROM trainings2 WHERE id = "'.$id.'"';
				$result = $mysqli->query($query);
				$row = $result->fetch_row();
				// m��rab, mis t��pi sisu edastatakse
				header('Content-Type: application/json');
				echo json_encode($row);
				break;
				
			case "findCertificateInfo":
				require_once '../conf.php';
				// v��rtustame muutuja id tore.js-st postitud
				$userId = $mysqli->real_escape_string($_POST['userId']);
				$trainingId = $mysqli->real_escape_string($_POST['trainingId']);
					
				$query = 'SELECT t.name as trainingName, u.firstname, u.lastname, t.start_date FROM trainings2_participants tp join trainings2 t on tp.trainings2_id = t.id join users u on u.id = tp.users_id WHERE tp.trainings2_id = "'.$trainingId.'" and tp.users_id = "'.$userId.'"';
				$result = $mysqli->query($query);
				$row = $result->fetch_row();
				// m��rab, mis t��pi sisu edastatakse
				header('Content-Type: application/json');
				echo json_encode($row);
				break;
				
			case "addParticipant":
				require_once '../conf.php';
				
				$userId = $mysqli->real_escape_string($_POST['userId']);
				$trainingId = $mysqli->real_escape_string($_POST['trainingId']);
				
				$query = "INSERT INTO trainings2_participants VALUES ('".$trainingId."', '".$userId."')";
				$mysqli->query($query) or die($query.'<br />'.$mysqli->error);
				break;
				
			case "removeParticipant":
				require_once '../conf.php';
					
				$userId = $mysqli->real_escape_string($_POST['userId']);
				$trainingId = $mysqli->real_escape_string($_POST['trainingId']);
					
				$query = "DELETE FROM trainings2_participants WHERE trainings2_id = ".$trainingId." and users_id = ".$userId;
				$mysqli->query($query) or die($query.'<br />'.$mysqli->error);
				break;
				
			case "deleteById":
				require_once '../conf.php';
	
				$id = $mysqli->real_escape_string($_POST['id']);
					
				$query = 'DELETE FROM trainings2 WHERE id = "'.$id.'"';
				$result = $mysqli->query($query);
				break;	
				// defaulti minnakse kui �hegi muu case'i sisse ei minda
			default:
				header ( 'Location: ../../../main.php?view=trainings2' );
		}
	} else {
		if ($mysqli->connect_errno) {
			echo "Failed to connect to MySQL: " . $mysqli->connect_errno;
		}
		
		// andmebaasi objekt, millest mysqli_fetch_array abil tehakse igast reast array
		// kui on n�iteks LIKE a%, siis ta otsib k�ik a-ga algavad (% t�hendab, et sealt edasi on suvaline)
		
		$searchValue = filter_input(INPUT_POST, 'searchValue', FILTER_SANITIZE_STRING);
		
		if ( $_SESSION['role'] == 3 ) {
			$result = $mysqli->query('SELECT trainings2.id, trainings2.name, trainings2.start_date, trainings2.end_date, trainings2.lecturer, trainings2.location, trainings2.duration, trainings2.user_id, COUNT(trainings2_participants.users_id) AS participants FROM trainings2 LEFT JOIN trainings2_participants ON trainings2.id=trainings2_participants.trainings2_id WHERE (name LIKE "%'.$searchValue.'%" OR location LIKE "%'.$searchValue.'%" OR lecturer LIKE "%'.$searchValue.'%") AND user_id = "'.$_SESSION['user_id'].'" GROUP BY trainings2.id');
		} else {
			$result = $mysqli->query('SELECT trainings2.id, trainings2.name, trainings2.start_date, trainings2.end_date, trainings2.lecturer, trainings2.location, trainings2.duration, trainings2.user_id, COUNT(trainings2_participants.users_id) AS participants FROM trainings2 LEFT JOIN trainings2_participants ON trainings2.id=trainings2_participants.trainings2_id WHERE (name LIKE "%'.$searchValue.'%" OR location LIKE "%'.$searchValue.'%" OR lecturer LIKE "%'.$searchValue.'%") GROUP BY trainings2.id');
		}
		
		$array = array();
		while($row = mysqli_fetch_array($result)) {
			$array[] = $row;
			
		}
	
		// siit tuleb container'i sisu (mis muidu oli enne main.php-s)
		include('trainings2_list.php');
	}
	
} else {

	header('Location: main.php?view=home');


}

?>