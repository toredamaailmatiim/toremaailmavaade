<div class="container">


	<div class="row">&nbsp;</div>


	<div class="row">&nbsp;</div>


	<div class="row">&nbsp;</div>


	<div class="row">&nbsp;</div>


	<div class="row">


		<div class="col-md-10">


			<div class="panel panel-default">


				<!-- Default panel contents -->


				<div class="panel-heading">


					<h2>Suvelaager</h2>


				</div>


				<div class="panel-body">


					<form class="navbar-form navbar-left" role="search" method="POST"


						action="main.php?view=summercamp">


						<div class="form-group">


							<input type="text" name="searchValue" class="form-control"


								placeholder="Otsi suvelaager">


						</div>


						<button type="submit" class="btn btn-default">Otsi</button>


					</form>


				</div>





				<!-- Table -->

				<div class="table-responsive">

				<table class="table table-hover">


					<thead>


						<tr>


							<th>#</th>


							<th>Nimi</th>


							<th>Toimumise aeg</th>


                            <th>Lõpp</th>


							<th>Juhataja</th>


							<th>Asukoht</th>


							<th>Osalejaid</th>


                                                        <th>Kirjeldus</th>


							<th></th>


							<th></th>


						</tr>


					</thead>


					<tbody>


						<?php


						// count nÄ†Ā¤itab massiivi pikkust, suurust


						for($i = 0; $i < count($camps_array); $i++) {


							/* . kasutatakse stringide kokku liitmiseks, nagu javas + kahe stringi vahel


							 siin toimub stringi echomine


							tore.js jooksutatakse juba main.php-s lÄ†Ā¤bi, seega saan seda juba siin kasutada*/


							if ( $_SESSION['role'] == 5 ) {


								$edit_button = '<button type="button" class="btn btn-info btn-sm" onclick="SummercampJs.openModalForm('.$camps_array[$i]['id'].')">Muuda andmeid</button>';


								$delete_button = '<button type="button" class="btn btn-info btn-sm" onclick="SummercampJs.remove('.$camps_array[$i]['id'].');">Kustuta suvelaager</button>';


							}


							


							echo '


								<tr>


									<th scope="row">' . $camps_array [$i] ['id'] . '</th>


									<td>' . $camps_array [$i] ['name'] . '</td>


									<td>' . $camps_array [$i] ['camp_begin'] . '</td>


                                    <td>' . $camps_array [$i] ['camp_end'] . '</td>


									<td>' . $camps_array [$i] ['supervisor'] . '</td>


									<td>' . $camps_array [$i] ['location'] . '</td>


									<td><a href="#" onclick="SummercampJs.viewParticipants('.$camps_array[$i]['id'].');">' . $camps_array [$i] ['participants'] . '</a></td>


                                                                        <td>' . $camps_array [$i] ['description'] . '</td>


									<td>'.$edit_button.'</td>


									<td>'.$delete_button.'</td>


								</tr>';


						}


						?>		


					</tbody>


				</table>
				</div>

			</div>


			<p>


				<?php if ( $_SESSION['role'] == 5 ) { ?>


				<a class="btn btn-lg btn-primary" data-toggle="modal" href="#addNewMainSummercampModal">Lisa uus


					Suvelaager</a>


				<?php } ?>


			</p>


		</div>


		


		<div class="col-md-10"></div>





	</div>





</div>


<!-- /container -->





<div class="modal fade" id="addNewMainSummercampModal" tabindex="-1"


	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">


	<div class="modal-dialog">


		<div class="modal-content">


			<div class="modal-header">


				<button type="button" class="close" data-dismiss="modal">


					<span aria-hidden="true">&times;</span>


					<span class="sr-only">X</span>


				</button>


				<h3 class="modal-title" id="myModalLabel">Registreeri uus


					suvelaager:</h3>


			</div>


			<div class="modal-body">





				<!-- vormides kasutatakse POST andmete "postimiseks"  -->


				<!-- action viitab, kuhu postimisel (vormi submittimisel) minnakse  -->


				<form method="POST" action="src/php/contents/summercamp.php?action=register">





					<div class="form-horizontal center">


						<input type="hidden" class="form-control" name="id" />


						<div class="form-group">


							<label class="col-md-5 control-label">Nimi:</label>


							<div class="col-md-4">


								<input type="text" class="form-control" name="name" required />


							</div>


						</div>


						<div class="form-group">


							<label class="col-md-5 control-label">Algus KuupÄ†Ā¤ev:</label>


							<div class="col-md-4">


								<input type="date" class="form-control" name="camp_begin" required />


							</div>


                                                        </div>


                                                        


						<div class="form-group">


							<label class="col-md-5 control-label">LÄ†Āµpu kuupÄ†Ā¤ev:</label>


							<div class="col-md-4">


								<input type="date" class="form-control" name="camp_end" required />


							</div>


						</div>


						<div class="form-group">


							<label class="col-md-5 control-label">Juhataja:</label>


							<div class="col-md-4">


								<input type="text" class="form-control" name="supervisor" required />


							</div>


						</div>


						<div class="form-group">


							<label class="col-md-5 control-label">Asukoht:</label>


							<div class="col-md-4">


								<input type="text" class="form-control" name="location" required />


							</div>


                                                        </div>


                                                        


						<div class="form-group">


							<label class="col-md-5 control-label">Kirjeldus:</label>


							<div class="col-md-4">


								<input type="text" class="form-control" name="description" required />


							</div>


						</div>


						<div class="form-group">


							<input type="submit" class="btn btn-success" name="save_button"


								value="Salvesta">


						</div>


					</div>


				</form>


			</div>


			<div class="modal-footer">


				<button type="button" class="btn btn-default" data-dismiss="modal">Sulge</button>


			</div>


		</div>


	</div>


</div>





<div class="modal fade" id="viewParticipants" tabindex="-1"


	role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">


	<div class="modal-dialog">


		<div class="modal-content">


			<div class="modal-header">


				<button type="button" class="close" data-dismiss="modal">


					<span aria-hidden="true">&times;</span>


					<span class="sr-only">X</span>


				</button>


				<h3 class="modal-title" id="myModalLabel2">Osalejad:</h3>


			</div>


			<div class="modal-body">


				<table class="table table-hover">


					<thead>


						<tr>


							<th>#</th>


							<th>Nimi</th>


							<th></th>

						</tr>


					</thead>


					<tbody id="usersTable">


					</tbody>


					<tfoot>


						<tr>


							<td>


								<input type="hidden" class="form-control" name="summercampId" />


							</td>


							<td>	


								<select id="userSelect" class="form-control" name="userId" required>


									<option value="">--- Vali kasutaja ---</option>


								</select>


							</td>


							<td>


								<input type="button" class="btn btn-success" name="save_button"


								value="Lisa osaleja" onclick="SummercampJs.addParticipant();">


							</td>


						</tr>


					</tfoot>


				</table>


			</div>


			<div class="modal-footer">


				<button type="button" class="btn btn-default" data-dismiss="modal">Sulge</button>


			</div>


		</div>


	</div>


</div>