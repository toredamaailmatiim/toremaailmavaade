<?php

$action = isset($_POST) && $_POST['action'] ? $_POST['action'] : '';
$value  = isset($_POST) && $_POST['value'] ? $_POST['value'] : '';
$id  = isset($_POST) && $_POST['id'] ? $_POST['id'] : '';

session_start();

if ($_SESSION['permissions'][5] == 1) {

	if(isset($_GET['action'])) {
		
		switch ($action) {
	    
			case 'save':
				if ($value != '') {
	            
	    			require_once '../conf.php';
		
					if ($mysqli->connect_errno) {
		
						echo "Failed to connect to MySQL: " . $mysqli->connect_errno;
		
					}

            		if ($id == "firstname") {
            			$query = "UPDATE users SET firstname = '".$value."' WHERE id = '".$_SESSION['user_id']."'";
						$mysqli->query($query) or die($query.'<br />'.$mysqli->error);
            		} else if ($id == "lastname") {
            			$query = "UPDATE users SET lastname = '".$value."' WHERE id = '".$_SESSION['user_id']."'";
						$mysqli->query($query) or die($query.'<br />'.$mysqli->error);
            		} else if ($id == "ss_number") {
            			$query = "UPDATE users SET ss_number = '".$value."' WHERE id = '".$_SESSION['user_id']."'";
						$mysqli->query($query) or die($query.'<br />'.$mysqli->error);
            		} else if ($id == "address") {
            			$query = "UPDATE users SET address = '".$value."' WHERE id = '".$_SESSION['user_id']."'";
						$mysqli->query($query) or die($query.'<br />'.$mysqli->error);
            		} else if ($id == "phone") {
            			$query = "UPDATE users SET phone = '".$value."' WHERE id = '".$_SESSION['user_id']."'";
						$mysqli->query($query) or die($query.'<br />'.$mysqli->error);
            		} else if ($id == "school") {
            			$query = "UPDATE users SET school = '".$value."' WHERE id = '".$_SESSION['user_id']."'";
						$mysqli->query($query) or die($query.'<br />'.$mysqli->error);
            		} else if ($id == "supervisor_name") {
            			$query = "UPDATE members SET supervisor_name = '".$value."' WHERE user_id = '".$_SESSION['user_id']."'";
						$mysqli->query($query) or die($query.'<br />'.$mysqli->error);
            		} else if ($id == "parent_name") {
            			$query = "UPDATE members SET parent_name = '".$value."' WHERE user_id = '".$_SESSION['user_id']."'";
						$mysqli->query($query) or die($query.'<br />'.$mysqli->error);
            		} else if ($id == "parent_email") {
            			$query = "UPDATE members SET parent_email = '".$value."' WHERE user_id = '".$_SESSION['user_id']."'";
						$mysqli->query($query) or die($query.'<br />'.$mysqli->error);
            		} else if ($id == "parent_phone") {
            			$query = "UPDATE members SET parent_phone = '".$value."' WHERE user_id = '".$_SESSION['user_id']."'";
						$mysqli->query($query) or die($query.'<br />'.$mysqli->error);
            		}
            		
            		echo "OK";
        		} else {
            		echo "ERROR";
        		}
        	break;

			default:
        			echo "ERROR";
        	break;
		}
		
	} else {
	
		if ($mysqli->connect_errno) {
			echo "Failed to connect to MySQL: " . $mysqli->connect_errno;
		}
		
		$result = $mysqli->query('SELECT users.id, users.username, 
	                 users.ss_number, users.firstname, 
	                 users.lastname, users.gender, 
	                 users.address, users.phone, users.role, 
	                 users.school, members.supervisor_name, 
	                 members.parent_name, members.parent_email, 
	                 members.parent_phone FROM users LEFT JOIN members 
	                 ON members.user_id=users.id WHERE users.id = "'.$_SESSION['user_id'].'"');
		$array1 = array();
		while($row = mysqli_fetch_array($result)) {
			$array1[] = $row;
		}
		
		include('profile_page.php');
			
	}

} else {

	header('Location: main.php?view=home');

}

?>