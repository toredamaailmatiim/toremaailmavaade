<?php

session_start();

if ($_SESSION['permissions'][4] == 1) {

	if(isset($_GET['action'])) {
		switch ( $_GET['action'] ) {
		
			// case on nagu tegusĆµna
			case "subsidy_add":
			
				require_once '../conf.php';
		
				if ($mysqli->connect_errno) {
		
					echo "Failed to connect to MySQL: " . $mysqli->connect_errno;
		
				}
						
				$email  = $mysqli->real_escape_string($_POST['isikuEmail']);
				$taotlemisetyyp  = $mysqli->real_escape_string($_POST['taotlemiseTyyp']);
				$nimi  = $mysqli->real_escape_string($_POST['nimi']);
				$summa = $mysqli->real_escape_string($_POST['Summa']);
				$informatsioon  = $mysqli->real_escape_string($_POST['Informatsioon']);
		
				/* echo aitab printida ja exit tapab koodi
				. on nagu + on Javas (Stringide liitmiseks)*/
				if($id == 0) {
					$query = "Insert into subsidy_application (email, taotlemisetyyp, nimi, summa, informatsioon, taotluse_aeg, rahuldatud)  Values('".$email."','".$taotlemisetyyp."','".$nimi."','".$summa."','".$informatsioon."', current_timestamp, 0)";
	                                $mysqli->query($query) or die($query.'<br />'.$mysqli->error);
				} else {
					$query = "UPDATE subsidy_application set email = '".$email."', taotlemisetyyp = '".$taotlemisetyyp."',nimi = '".$nimi."', summa = '".$summa."', informatsioon = '".$informatsioon."' WHERE id = '".$id."'";
					$mysqli->query($query) or die($query.'<br />'.$mysqli->error);
				}
		
				header ( 'Location: ../../../main.php?view=subsidy' );	
			
			case "findById":
				require_once '../conf.php';
				// vĆ¤Ć¤rtustame muutuja id tore.js-st postitud
				$id = $mysqli->real_escape_string($_POST['id']);
				
				$query = 'SELECT * FROM subsidy_application WHERE id = "'.$id.'"';
				$result = $mysqli->query($query);
				$row = $result->fetch_row();
				// mĆ¤Ć¤rab, mis tĆ¼Ć¼pi sisu edastatakse
				header('Content-Type: application/json');
				echo json_encode($row);
				break;
	                
	                case "approveById":
				require_once '../conf.php';
				// vĆ¤Ć¤rtustame muutuja id tore.js-st postitud
				$id = $mysqli->real_escape_string($_POST['id']);
				
				$query = "UPDATE subsidy_application set rahuldatud = 1 where id = '".$id."'";
				$result = $mysqli->query($query);
	                        header ( 'Location: ../../../main.php?view=subsidy' );
				break;
	                    
			case "deleteById":
				require_once '../conf.php';
	
				$id = $mysqli->real_escape_string($_POST['id']);
					
				$query = 'DELETE FROM subsidy_application WHERE id = "'.$id.'"';
				$result = $mysqli->query($query);
				break;	
				// defaulti minnakse kui Ć¼hegi muu case'i sisse ei minda
			default:
				header ( 'Location: ../../../main.php?view=subsidy' );
		}
	} else {
		if ($mysqli->connect_errno) {
			echo "Failed to connect to MySQL: " . $mysqli->connect_errno;
		}
		
		// andmebaasi objekt, millest mysqli_fetch_array abil tehakse igast reast array
		// kui on nĆ¤iteks LIKE a%, siis ta otsib kĆµik a-ga algavad (% tĆ¤hendab, et sealt edasi on suvaline)
		
		$searchValue = filter_input(INPUT_POST, 'searchValue', FILTER_SANITIZE_STRING);
		
		$result = $mysqli->query('SELECT subsidy_application.id, subsidy_application.nimi, subsidy_application.informatsioon, subsidy_application.summa, subsidy_application.email, subsidy_application.taotlemisetyyp, subsidy_application.rahuldatud FROM subsidy_application WHERE
	                 (email LIKE "%'.$searchValue.'%" OR
	                 taotlemisetyyp LIKE "%'.$searchValue.'%" OR 
	                 nimi LIKE "%'.$searchValue.'%") GROUP BY subsidy_application.id');
		
	        
		$array = array();
		while($row = mysqli_fetch_array($result)) {
			$array[] = $row;
		}
		
		include('subsidy_page.php');
	       
	
		// siit tuleb container'i sisu (mis muidu oli enne main.php-s)
	}
	
} else {

	header('Location: main.php?view=home');

}	
	
?>