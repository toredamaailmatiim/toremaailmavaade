<!-- Põhikooltuste lehe vaate container'i sisu, millele viidatakse trainings.php-s -->

<div class="container">
	<div class="row">&nbsp;</div>
	<div class="row">&nbsp;</div>
	<div class="row">&nbsp;</div>
	<div class="row">&nbsp;</div>
	<div class="row">
		<div class="col-md-9">
			<div class="panel panel-default">
				<!-- Default panel contents -->
				<div class="panel-heading">
					<h2>Põhikoolitused</h2>
				</div>
				<div class="panel-body">
					<form class="navbar-form navbar-left" role="search" method="POST"
						action="main.php?view=trainings">
						<div class="form-group">
							<input type="text" name="searchValue" class="form-control"
								placeholder="Otsi põhikoolitust">
						</div>
						<button type="submit" class="btn btn-default">Otsi</button>
					</form>
				</div>

				<!-- Table -->
				<div class="table-responsive">
				<table class="table table-hover">
					<thead>
						<tr>
							<th>#</th>
							<th>Nimi</th>
							<th>Toimumise algus</th>
							<th>Toimumise lõpp</th>
							<th>Koolitaja</th>
							<th>Asukoht</th>
							<th>Kestus</th>
							<th>Osalejaid</th>
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<?php
						// count näitab massiivi pikkust, suurust
						for($i = 0; $i < count ( $array ); $i ++) {
							/* . kasutatakse stringide kokku liitmiseks, nagu javas + kahe stringi vahel
							 siin toimub stringi echomine
							tore.js jooksutatakse juba main.php-s läbi, seega saan seda juba siin kasutada*/
							echo '
								<tr>
									<th scope="row">' . $array [$i] ['id'] . '</th>
									<td>' . $array [$i] ['name'] . '</td>
									<td>' . $array [$i] ['start_date'] . '</td>
									<td>' . $array [$i] ['end_date'] . '</td>
									<td>' . $array [$i] ['lecturer'] . '</td>
									<td>' . $array [$i] ['location'] . '</td>
									<td>' . $array [$i] ['duration'] . '</td>
									<td><a id="participants-'.$array [$i] ['id'].'" href="#" onclick="Training.viewParticipants('.$array[$i]['id'].');">' . $array [$i] ['participants'] . '</a></td>
									<td><button type="button" class="btn btn-info btn-sm" onclick="Training.openModalForm('.$array[$i]['id'].')">Muuda andmeid</button></td>
									<td><button type="button" class="btn btn-info btn-sm" onclick="Training.remove('.$array[$i]['id'].');">Kustuta koolitus</button></td>
								</tr>';
						}
						?>		
					</tbody>
				</table>
				</div>
				</div>
			<p>
				<a class="btn btn-lg btn-primary" onclick="Training.openModalForm()" role="button">Lisa uus
					põhikoolitus</a>
			</p>
		</div>
		<div class="col-md-3 blog-sidebar">
			<div class="sidebar-module">
				<h2>Raha taotlused</h2>
				<p>Taotle raha oma koolitusele</p>
				<p>
					<a class="btn btn-default" href="main.php?view=raha_taotlus"
						role="button">Vaata lähemalt &raquo;</a>
				</p>
			</div>
		</div>
		<!-- /.blog-sidebar -->
		<div class="col-md-9"></div>

	</div>

</div>
<!-- /container -->

<div class="modal fade" id="addNewMainTrainingModal" tabindex="-1"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span>
					<span class="sr-only">X</span>
				</button>
				<h3 class="modal-title" id="myModalLabel">Registreeri uus
					põhikoolitus:</h3>
			</div>
			<div class="modal-body">

				<!-- vormides kasutatakse POST andmete "postimiseks"  -->
				<!-- action viitab, kuhu postimisel (vormi submittimisel) minnakse  -->
				<form method="POST"
					action="src/php/contents/trainings.php?action=register">

					<div class="form-horizontal center">
						<input type="hidden" class="form-control" name="id" />
						<div class="form-group">
							<label class="col-md-5 control-label">Nimi:</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="name" required />
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-5 control-label">Toimumise algus:</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="start_date" required />
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-5 control-label">Toimumise lõpp:</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="end_date" required />
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-5 control-label">Koolitaja:</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="lecturer" required />
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-5 control-label">Asukoht:</label>
							<div class="col-md-4">
								<input type="text" class="form-control" name="location" required />
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-5 control-label">Kestus:</label>
							<div class="col-md-4">
								<input type="number" class="form-control" name="duration" required />
							</div>
						</div>
						<div class="form-group">
							<input type="submit" class="btn btn-success" name="save_button"
								value="Salvesta">
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Sulge</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="viewParticipants" tabindex="-1"
	role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span>
					<span class="sr-only">X</span>
				</button>
				<h3 class="modal-title" id="myModalLabel2">Osalejad:</h3>
			</div>
			<div class="modal-body">
				<table class="table table-hover">
					<thead>
						<tr>
							<th>#</th>
							<th>Nimi</th>
							<th></th>
						</tr>
					</thead>
					<tbody id="usersTable">
					</tbody>
					<tfoot>
						<tr>
							<td>
								<input type="hidden" class="form-control" name="trainingId" />
							</td>
							<td>	
								<select id="userSelect" class="form-control" name="userId" required size="1">
									<option value="">Vali kasutaja</option>
								</select>
							</td>
							<td>
								<input type="button" class="btn btn-success" name="save_button"
								value="Lisa osaleja" onclick="Training.addParticipant();">
							</td>
						</tr>
					</tfoot>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Sulge</button>
			</div>
		</div>
	</div>
</div>