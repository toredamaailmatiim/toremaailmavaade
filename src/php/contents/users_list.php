    <div class="container-fluid">
        <div class="row">&nbsp;</div>
        <div class="row">&nbsp;</div>
        <div class="row">&nbsp;</div>
        <div class="row">&nbsp;</div>
        <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <!-- Default panel contents -->
                <div class="panel-heading"><h2>Kasutajad</h2></div>
                <div class="panel-body">
                  <form class="navbar-form navbar-left" role="search">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Otsi kasutajat">
        </div>
        <button type="submit" class="btn btn-default">Otsi</button>
      </form>
                </div>

                <!-- Table -->
                <div class="table-responsive">
                <table class="table table-hover">
                  <thead>
        <tr>
          <th>#</th>
          <th>Roll</th>
          <th>Sugu</th>
          <th>Eesnimi</th>
          <th>Perekonnanimi</th>
          <th>E-mail</th>
          <th>Isikukood</th>
          <th>Elukoha aadress</th>
          <th>Telefon</th>
          <th>Kool</th>
          <th>Juhendaja nimi</th>
          <th>Vanema nimi</th>
          <th>Vanema e-mail</th>  
          <th>Vanema telefon</th>      
          <!--<th>Muud andmed</th>-->
        </tr>
      </thead>
      <tbody>
		<?php
               
		for($i = 0; $i < count ( $array1 ); $i ++) {
                    
			/* . kasutatakse stringide kokku liitmiseks, nagu javas + kahe stringi vahel
                         siin toimub stringi echomine
			tore.js jooksutatakse juba main.php-s läbi, seega saan seda juba siin kasutada*/
				echo '
					<tr>
						<th scope="row">' . $array1 [$i] ['id'] . '</th>
                                                <td>' . $array1 [$i] ['role'] . '</td>
						<td>' . $array1 [$i] ['gender'] . '</td>
						<td>' . $array1 [$i] ['firstname'] . '</td>
                                                <td>' . $array1 [$i] ['lastname'] . '</td>
						<td>' . $array1 [$i] ['username'] . '</td>
						<td>' . $array1 [$i] ['ss_number'] . '</td>
                                                <td>' . $array1 [$i] ['address'] . '</td>
						<td>' . $array1 [$i] ['phone'] . '</td>
                                                <td>' . $array1 [$i] ['school'] . '</td>
						<td>' . $array1 [$i] ['supervisor_name'] . '</td>
						<td>' . $array1 [$i] ['parent_name'] . '</td>
                                                <td>' . $array1 [$i] ['parent_email'] . '</td>
                                                <td>' . $array1 [$i] ['parent_phone'] . '</td>
						<td></td>
					</tr>';
			}
			?>
      </tbody>
                </table>
                </div>
                <div class="panel-footer">
                    <ul class="pagination pagination-sm">
        <li>
          <a href="#" aria-label="Previous">
            <span aria-hidden="true">&laquo;</span>
          </a>
        </li>
        <li><a href="#">1</a></li>
        <li><a href="#">2</a></li>
        <li><a href="#">3</a></li>
        <li>
          <a href="#" aria-label="Next">
            <span aria-hidden="true">&raquo;</span>
          </a>
        </li>
      </ul>
                </div>
              </div>
             <?php if ( $_SESSION['role'] == 5 ) { ?>
            <p><a class="btn btn-lg btn-primary" data-toggle="modal" data-target="#addNewUserModal" role="button">Uus kasutaja</a></p>
            <?php } ?>
        </div>
            
            <div class="col-md-12"></div>
            
        </div>

    </div> <!-- /container -->
    
	<div class="modal fade" id="addNewUserModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">X</span></button>
					<h3 class="modal-title" id="myModalLabel">Registreeri uus kasutaja:</h3>
				</div>
				<div class="modal-body">
					<form method="POST" action="?action=add">
						<div class="form-horizontal">
							<div class="form-group">
								<label class="col-md-5 control-label">Eesnimi:</label>
								<div class="col-md-4">
									<input type="text" class="form-control" name="firstName" required/>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-5 control-label">Perekonnanimi:</label>
								<div class="col-md-4">
									<input type="text" class="form-control" name="lastName" required/>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-5 control-label">E-mail:</label>
								<div class="col-md-4">
									<input type="email" class="form-control" name="email" required/>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-5 control-label">Isikukood:</label>
								<div class="col-md-4">
									<input type="text" class="form-control" name="SSNumber"/>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-5 control-label">Elukoha aadress:</label>
								<div class="col-md-4">
									<input type="text" class="form-control" name="address"/>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-5 control-label">Telefon:</label>
								<div class="col-md-4">
									<input type="text" class="form-control" name="phone"/>
								</div>
							</div>
							<div class="form-group">
									<label class="col-md-5 control-label">Roll</label>
									<div class="col-md-4">
									<select size="1" name="role" class="form-control">
											<option selected value="1">Liige (laps)</option>
											<option value="2">Juhendaja</option>
															<option value="3">Koolitaja</option>
															<option value="4">Vilistlane</option>
															<option value="5">Administraator</option>
															<option value="6">Registreeritud kasutaja</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<input type="submit" class="btn btn-success" name="save_button" value="Salvesta">
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Sulge</button>
				</div>
			</div>
		</div>
	</div>  
	
