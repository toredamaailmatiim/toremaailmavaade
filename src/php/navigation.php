<?php

?>
<nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
      
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">TORE</a>
        </div>
        
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
          
          <!-- kui view parameeter on v�rdne, siis pane class active k�lge. Algselt on view parameeter t�hi. -->
          <!-- et navigeerimise paneelil vastava alamlehe nimi, kus parajasti toimetatakse, oleks taustalt tume -->
            <li <?php echo $view == '' ? 'class="active"' : ''; ?>><a href="main.php">Esileht</a></li>
            <?php if ($_SESSION['permissions'][0] == 1) { ?><li <?php echo $view == 'users' ? 'class="active"' : ''; ?>><a href="main.php?view=users">Kasutajad</a></li><?php } ?>
            <?php if ($_SESSION['permissions'][1] == 1) { ?><li <?php echo $view == 'trainings' ? 'class="active"' : ''; ?>><a href="main.php?view=trainings">P�hikoolitused</a></li><?php } ?>
            <?php if ($_SESSION['permissions'][2] == 1) { ?><li <?php echo $view == 'trainings2' ? 'class="active"' : ''; ?>><a href="main.php?view=trainings2">T�iendkoolitused</a></li><?php } ?>
            <?php if ($_SESSION['permissions'][3] == 1) { ?><li <?php echo $view == 'summercamp' ? 'class="active"' : ''; ?>><a href="main.php?view=summercamp">Suvelaagrid</a></li><?php } ?>
			<?php if ($_SESSION['permissions'][4] == 1) { ?><li <?php echo $view == 'subsidy' ? 'class="active"' : ''; ?>><a href="main.php?view=subsidy">Raha taotlused</a></li><?php } ?>
			<?php if ($_SESSION['permissions'][6] == 1) { ?><li <?php echo $view == 'general_meeting' ? 'class="active"' : ''; ?>><a href="main.php?view=general_meeting">�ldk-d</a></li><?php } ?>
          </ul>
          <ul class="nav navbar-nav navbar-right">
          	<li><a href="#">Roll: <?php echo $role; ?></a></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">&lt; <?php echo $_SESSION['username']; ?> &gt;<span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li <?php echo $view == 'profile' ? 'class="active"' : ''; ?>><a href="main.php?view=profile">Profiil</a></li>
                <!--<li><a href="#">Seaded</a></li>-->
                <li class="divider"></li>
                <li><a href="src/php/login.php?action=logOUT">Logi v�lja</a></li>
              </ul>
            </li>
          </ul>
        </div><!--/.nav-collapse -->
        
      </div>
    </nav>