$(function(){
    $.ajaxSetup({
        url: 'src/php/contents/profile.php?action=edit',
        type: 'POST',
        async: false,
        timeout: 500
    });
    $('.editable').inlineEdit({
        save: function(event, data) {
            var html = $.ajax({
                data: { 'action': 'save', 'id': this.id, 'value': data.value }
            }).responseText;
            
            return html === 'OK' ? true : false;
        }
    });
});
