

var General_MeetingJs = {
		//trainingId väärtus antakse kaasa sellest failist, kus seda kasutatakse
	openModalForm: function(general_meetingId) {

		//kui trainingId on olemas
		if(general_meetingId) {
			// ajaxi väljakutsumine
                        
			$.ajax({
				method: "POST",
				//url määrab, kuhu postitakse
			  	url: "src/php/contents/general_meeting.php?action=findById",
			  	// data näitab, mida postitakse ehk postitakse (json tüüpi) objekt mille üks väli id
			  	data: { id: general_meetingId }
			})
			// done garanteerib, et seda tegevust siin tehakse alles siis, kui ajax on tehtud
			// result on massiiv, mis echotakse ja kus on ühe andmerea andmed massiivina
			.done(function(result) {
				// otsitakse html-st input väli, kus id on name
				$('input[name=id]').val(result[0]);
				$('input[name=name]').val(result[1]);
				$('input[name=camp_begin]').val(result[2]);
				$('input[name=camp_end]').val(result[3]);
				$('input[name=supervisor]').val(result[4]);
                                $('input[name=location]').val(result[5]);
                                $('input[name=description]').val(result[7]);
				$('#addnewMainGeneralMeetingModal').modal('show');
				document.location.hash = "editGen_Met-" + general_meetingId;
			});
			
			//trainingId pole olemas - ehk olukord 2, kui hakatakse looma uut koolitust
		} else {
                    
			$('input[name=id]').val(0);
			$('input[name=email]').val('');
			$('input[name=taotlemisetyyp]').val('');
			$('input[name=nimi]').val('');
                        $('input[name=summa]').val('');
                        $('input[name=informatsioon]').val('');
			$('#addnewMainGeneralMeetingModal').modal('show');
			document.location.hash = "addGen_Met";
		}
	},
	              
        viewParticipants: function(general_meetingId) {
		$.ajax({
			method: "POST",
			//url määrab, kuhu postitakse
		  	url: "src/php/contents/users.php?action=getGeneralMeetingUsers",
		  	// data näitab, mida postitakse ehk postitakse (json tüüpi) objekt mille üks väli id
		  	data: { general_meetingId: general_meetingId }
		})
		// done garanteerib, et seda tegevust siin tehakse alles siis, kui ajax on tehtud
		// result on massiiv, mis echotakse ja kus on ühe andmerea andmed massiivina
		.done(function(result) {
			var dropdownUsers = result.not_registered;
			var registeredUsers = result.registered;
			
			// draw dropdown
			var select = $('#userSelect');
			$(select).empty();
			$('<option>').val(0).text("--- Vali kasutaja ---").appendTo(select);
			$.each(dropdownUsers, function(key, value) {              
		        $('<option>').val(value.id).text(value.firstname + " " + value.lastname).appendTo(select);     
		    });
			
			// draw table
			var table = $('#usersTable');
			$(table).empty();
			$.each(registeredUsers, function(key, value) {
				var row = '<tr>';
				row += '<td scope="row">' + value.id + '</td>';
				row += '<td>'+value.firstname + " " + value.lastname +'</td>';
				row += '<td><button type="button" class="btn btn-info btn-sm" onclick="General_MeetingJs.removeParticipant('+value.id+','+general_meetingId+');">Eemalda</button></td>';
				row += '</tr>';
		        $(table).append(row);
		    });
			
			
			$('input[name=general_meetingId]').val(general_meetingId);
			$('#viewParticipants').modal('show');
		});
	},
	
	addParticipant: function() {
		var userId = $('#userSelect').val();
		var general_meetingId = $('input[name=general_meetingId]').val();
		
		$.ajax({
			method: "POST",
		  	url: "src/php/contents/general_meeting.php?action=addParticipant",
		  	data: { general_meetingId: general_meetingId, userId: userId }
		})
		.done(function(result) {
			console.log("testing, added?");
			// TODO: refresh page
			General_meetingJs.viewParticipants(general_meetingId);
		});
	},
	
	removeParticipant: function(userId, general_meetingId) {		
		$.ajax({
			method: "POST",
		  	url: "src/php/contents/general_meeting.php?action=removeParticipant",
		  	data: { general_meetingId: general_meetingId, userId: userId }
		})
		.done(function(result) {
			console.log("testing, removed?");
			//TOOD: refresh page
			General_MeetingJS.viewParticipants(general_meetingId);
		});
	},
        
        remove: function(general_meetingId) {
		var confirm = window.confirm("Kustuta Uldkoosolek?");
		if(confirm) {
			$.ajax({
                            
				method: "POST",
			  	url: "src/php/contents/general_meeting.php?action=deleteById",
			  	data: { id: general_meetingId }
			})
			.done(function() {
				window.location = 'main.php?view=general_meeting';
                    });
}}
        };
var SubsidyJs = {
		//trainingId väärtus antakse kaasa sellest failist, kus seda kasutatakse
	openModalForm: function(subsidyId) {

		//kui trainingId on olemas
		if(subsidyId) {
			// ajaxi väljakutsumine
                        
			$.ajax({
				method: "POST",
				//url määrab, kuhu postitakse
			  	url: "src/php/contents/subsidy.php?action=findById",
			  	// data näitab, mida postitakse ehk postitakse (json tüüpi) objekt mille üks väli id
			  	data: { id: subsidyId }
			})
			// done garanteerib, et seda tegevust siin tehakse alles siis, kui ajax on tehtud
			// result on massiiv, mis echotakse ja kus on ühe andmerea andmed massiivina
			.done(function(result) {
				// otsitakse html-st input väli, kus id on name
				$('input[name=id]').val(result[0]);
				$('input[name=email]').val(result[1]);
				$('input[name=taotlemisetyyp]').val(result[2]);
				$('input[name=nimi]').val(result[3]);
				$('input[name=summa]').val(result[4]);
                                $('input[name=informatsioon]').val(result[5]);
				$('#addnewMainSubsidyModal').modal('show');
				document.location.hash = "editSubsidy-" + subsidyId;
			});
			
			//trainingId pole olemas - ehk olukord 2, kui hakatakse looma uut koolitust
		} else {
                    
			$('input[name=id]').val(0);
			$('input[name=email]').val('');
			$('input[name=taotlemisetyyp]').val('');
			$('input[name=nimi]').val('');
                        $('input[name=summa]').val('');
                        $('input[name=informatsioon]').val('');
			$('#addnewMainsubsidyModal').modal('show');
			document.location.hash = "addSubsidy";
		}
	},
	
	remove: function(subsidyId) {
		var confirm = window.confirm("Kustuta taotlus?");
		if(confirm) {
			$.ajax({
                            
				method: "POST",
			  	url: "src/php/contents/subsidy.php?action=deleteById",
			  	data: { id: subsidyId }
			})
			.done(function() {
				window.location = 'main.php?view=subsidy';
			});
		}
	},
        
        approve: function(subsidyId) {
		var confirm = window.confirm("Kinnita taotlus?");
		if(confirm) {
			$.ajax({
                            
				method: "POST",
			  	url: "src/php/contents/subsidy.php?action=approveById",
			  	data: { id: subsidyId }
			})
			.done(function() {
				window.location = 'main.php?view=subsidy';
			});
		}
            }
};

//-----------------------
var SummercampJs = {
		//trainingId väärtus antakse kaasa sellest failist, kus seda kasutatakse
	openModalForm: function(summercampId) {

		//kui trainingId on olemas
		if(summercampId) {
			// ajaxi väljakutsumine
                        
			$.ajax({
				method: "POST",
				//url määrab, kuhu postitakse
			  	url: "src/php/contents/summercamp.php?action=findById",
			  	// data näitab, mida postitakse ehk postitakse (json tüüpi) objekt mille üks väli id
			  	data: { id: summercampId }
			})
			// done garanteerib, et seda tegevust siin tehakse alles siis, kui ajax on tehtud
			// result on massiiv, mis echotakse ja kus on ühe andmerea andmed massiivina
			.done(function(result) {
				// otsitakse html-st input väli, kus id on name
				$('input[name=id]').val(result[0]);
				$('input[name=name]').val(result[1]);
				$('input[name=camp_begin]').val(result[2]);
				$('input[name=camp_end]').val(result[3]);
				$('input[name=supervisor]').val(result[4]);
                                $('input[name=location]').val(result[5]);
                                $('input[name=description]').val(result[7]);
				$('#addnewMainSummercampModal').modal('show');
				document.location.hash = "editSummercamp-" + summercampId;
			});
			
			//trainingId pole olemas - ehk olukord 2, kui hakatakse looma uut koolitust
		} else {
			$('input[name=id]').val(0);
			$('input[name=name]').val('');
			$('input[name=camp_begin]').val('');
			$('input[name=camp_end]').val('');
			$('input[name=supervisor]').val('');
                        $('input[name=location]').val('');
                        $('input[name=description]').val('');
			$('#addewMainSummercampModal').modal('show');
			document.location.hash = "addSummercamp";
		}
	},
	
	remove: function(summercampId) {
		var confirm = window.confirm("Kustuta suvelaager?");
		if(confirm) {
			$.ajax({
				method: "POST",
			  	url: "src/php/contents/summercamp.php?action=deleteById",
			  	data: { id: summercampId }
			})
			.done(function() {
				window.location = 'main.php?view=summercamp';
			});
		}
	},
	
	viewParticipants: function(summercampId) {
		$.ajax({
			method: "POST",
			//url määrab, kuhu postitakse
		  	url: "src/php/contents/users.php?action=getSummercampUsers",
		  	// data näitab, mida postitakse ehk postitakse (json tüüpi) objekt mille üks väli id
		  	data: { summercampId: summercampId }
		})
		// done garanteerib, et seda tegevust siin tehakse alles siis, kui ajax on tehtud
		// result on massiiv, mis echotakse ja kus on ühe andmerea andmed massiivina
		.done(function(result) {
			var dropdownUsers = result.not_registered;
			var registeredUsers = result.registered;
			
			// draw dropdown
			var select = $('#userSelect');
			$(select).empty();
			$('<option>').val(0).text("--- Vali kasutaja ---").appendTo(select);
			$.each(dropdownUsers, function(key, value) {              
		        $('<option>').val(value.id).text(value.firstname + " " + value.lastname).appendTo(select);     
		    });
			
			// draw table
			var table = $('#usersTable');
			$(table).empty();
			$.each(registeredUsers, function(key, value) {
				var row = '<tr>';
				row += '<td scope="row">' + value.id + '</td>';
				row += '<td>'+value.firstname + " " + value.lastname +'</td>';
				row += '<td><button type="button" class="btn btn-info btn-sm" onclick="SummercampJs.removeParticipant('+value.id+','+summercampId+');">Eemalda</button></td>';
				row += '</tr>';
		        $(table).append(row);
		    });
			
			
			$('input[name=summercampId]').val(summercampId);
			$('#viewParticipants').modal('show');
		});
	},
	
	addParticipant: function() {
		var userId = $('#userSelect').val();
		var summercampId = $('input[name=summercampId]').val();
		
		$.ajax({
			method: "POST",
		  	url: "src/php/contents/summercamp.php?action=addParticipant",
		  	data: { summercampId: summercampId, userId: userId }
		})
		.done(function(result) {
			console.log("testing, added?");
			// TODO: refresh page
			SummercampJs.viewParticipants(summercampId);
		});
	},
	
	removeParticipant: function(userId, summercampId) {		
		$.ajax({
			method: "POST",
		  	url: "src/php/contents/summercamp.php?action=removeParticipant",
		  	data: { summercampId: summercampId, userId: userId }
		})
		.done(function(result) {
			console.log("testing, removed?");
			//TOOD: refresh page
			SummercampJs.viewParticipants(summercampId);
		});
	}
};
var Training = {
		//trainingId väärtus antakse kaasa sellest failist, kus seda kasutatakse
	openModalForm: function(trainingId) {

		//kui trainingId on olemas
		if(trainingId) {
			// ajaxi väljakutsumine
			$.ajax({
				method: "POST",
				//url määrab, kuhu postitakse
			  	url: "src/php/contents/trainings.php?action=findById",
			  	// data näitab, mida postitakse ehk postitakse (json tüüpi) objekt mille üks väli id
			  	data: { id: trainingId }
			})
			// done garanteerib, et seda tegevust siin tehakse alles siis, kui ajax on tehtud
			// result on massiiv, mis echotakse ja kus on ühe andmerea andmed massiivina
			.done(function(result) {
				// otsitakse html-st input väli, kus id on name
				$('input[name=id]').val(result[0]);
				$('input[name=name]').val(result[1]);
				$('input[name=lecturer]').val(result[2]);
				$('input[name=start_date]').val(result[3]);
				$('input[name=end_date]').val(result[4]);
				$('input[name=location]').val(result[5]);
				$('input[name=duration]').val(result[6]);
				$('#addNewMainTrainingModal').modal('show');
				document.location.hash = "editTraining-" + trainingId;
			});
			
			//trainingId pole olemas - ehk olukord 2, kui hakatakse looma uut koolitust
		} else {
			$('input[name=id]').val(0);
			$('input[name=name]').val('');
			$('input[name=start_date]').val('');
			$('input[name=end_date]').val('');
			$('input[name=lecturer]').val('');
			$('input[name=location]').val('');
			$('input[name=duration]').val('');
			$('#addNewMainTrainingModal').modal('show');
			document.location.hash = "addTraining";
		}
	},
	
	remove: function(trainingId) {
		var confirm = window.confirm("Kustuta koolitus?");
		if(confirm) {
			$.ajax({
				method: "POST",
			  	url: "src/php/contents/trainings.php?action=deleteById",
			  	data: { id: trainingId }
			})
			.done(function() {
				window.location = 'main.php?view=trainings';
			});
		}
	},
	
	viewParticipants: function(trainingId) {
		$.ajax({
			method: "POST",
			//url määrab, kuhu postitakse
		  	url: "src/php/contents/users.php?action=getTrainingUsers",
		  	// data näitab, mida postitakse ehk postitakse (json tüüpi) objekt mille üks väli id
		  	data: { trainingId: trainingId }
		})
		// done garanteerib, et seda tegevust siin tehakse alles siis, kui ajax on tehtud
		// result on massiiv, mis echotakse ja kus on ühe andmerea andmed massiivina
		.done(function(result) {
			var dropdownUsers = result.not_registered;
			var registeredUsers = result.registered;
			
			// draw dropdown
			var select = $('#userSelect');
			$(select).empty();
			$('<option>').val(0).text("--- Valid kasutaja ---").appendTo(select);
			$.each(dropdownUsers, function(key, value) {              
		        $('<option>').val(value.id).text(value.firstname + " " + value.lastname).appendTo(select);     
		    });
			
			// draw table
			var table = $('#usersTable');
			var count = 0;
			$(table).empty();
			$.each(registeredUsers, function(key, value) {
				var row = '<tr>';
				row += '<td scope="row">' + value.id + '</td>';
				row += '<td>'+value.firstname + " " + value.lastname +'</td>';
				row += '<td><button type="button" class="btn btn-info btn-sm" onclick="Training.removeParticipant('+value.id+','+trainingId+');">Eemalda</button></td>';
				row += '<td><button type="button" class="btn btn-info btn-sm" onclick="Certificate.pdf('+value.id+',' + trainingId+');">Väljasta tunnistus</button></td>';
				row += '</tr>';
		        $(table).append(row);
		        count++;
		    });
			$('#participants-'+trainingId).html(count);
			
			$('input[name=trainingId]').val(trainingId);
			$('#viewParticipants').modal('show');
		});
	},
	
	addParticipant: function() {
		var userId = $('#userSelect').val();
		var trainingId = $('input[name=trainingId]').val();
		
		$.ajax({
			method: "POST",
		  	url: "src/php/contents/trainings.php?action=addParticipant",
		  	data: { trainingId: trainingId, userId: userId }
		})
		.done(function(result) {
			console.log("testing, added?");
			// TODO: refresh page
			Training.viewParticipants(trainingId);
		});
	},
	
	removeParticipant: function(userId, trainingId) {		
		$.ajax({
			method: "POST",
		  	url: "src/php/contents/trainings.php?action=removeParticipant",
		  	data: { trainingId: trainingId, userId: userId }
		})
		.done(function(result) {
			console.log("testing, removed?");
			//TOOD: refresh page
			Training.viewParticipants(trainingId);
		});
	}
};

var History = {
	
	recentHash: "",
		
	checkHash: function() {
		var hash = document.location.hash;
		if (hash) {
			hash = hash.substr(1);
			if (hash == History.recentHash) {
				return;
		    }
			History.recentHash = hash;
		    History.loadPage(hash);
		} else {
			//$('.modal').modal('hide');
			$('#addNewMainTrainingModal').modal('hide');
			$('#addNewMainTrainingModal2').modal('hide');
		} 
	},
	
	loadPage: function(hash) {
		if(hash.indexOf("editTraining2") === 0) {
			var id = parseInt(hash.split("-")[1]);
			Training2.openModalForm(id);	
		} else if(hash.indexOf("addTraining2") === 0) {
			Training2.openModalForm();	
		} else if(hash.indexOf("editTraining") === 0) {
			var id = parseInt(hash.split("-")[1]);
			Training.openModalForm(id);		
		} else if(hash.indexOf("addTraining") === 0) {
			Training.openModalForm();	
		}
	}
};

var Certificate = {
	
	pdf: function(userId, trainingId) {
		
		$.ajax({
			method: "POST",
		  	url: "src/php/contents/trainings.php?action=findCertificateInfo",
		  	data: { trainingId: trainingId, userId: userId }
		})
		.done(function(result) {
			console.log("testing, removed?", result);
			
			var doc = new jsPDF();
			
			doc.setFontSize(12);
			doc.text(50, 20, 'NÜ Tugiõpilaste Oma Ring Eestis');

			doc.setFontSize(26);
			doc.text(70, 70, 'TUNNISTUS nr. X');
			
			doc.setFontSize(26);
			doc.text(60, 100, result[1] + ' ' + result[2]);
			
			doc.setFontSize(12);
			doc.text(20, 140, 'On läbinud T.O.R.E ' + result[0] + ' alase koolituse mahus X h perioodil ' + result[3]);
			
			doc.setFontSize(12);
			doc.text(20, 160, 'Välja antud');

			doc.save('Test.pdf');

			
		});
	}
};


// Trainings2 jaoks asjad (kas on ikka vaja eraldi?)

var Training2 = {
		//trainingId väärtus antakse kaasa sellest failist, kus seda kasutatakse
	openModalForm: function(trainingId) {

		//kui trainingId on olemas
		if(trainingId) {
			// ajaxi väljakutsumine
			$.ajax({
				method: "POST",
				//url määrab, kuhu postitakse
			  	url: "src/php/contents/trainings2.php?action=findById",
			  	// data näitab, mida postitakse ehk postitakse (json tüüpi) objekt mille üks väli id
			  	data: { id: trainingId }
			})
			// done garanteerib, et seda tegevust siin tehakse alles siis, kui ajax on tehtud
			// result on massiiv, mis echotakse ja kus on ühe andmerea andmed massiivina
			.done(function(result) {
				console.log(result);
				// otsitakse html-st input väli, kus id on name
				$('input[name=id]').val(result[0]);
				$('input[name=name]').val(result[1]);
				$('input[name=lecturer]').val(result[2]);
				$('input[name=start_date]').val(result[3]);
				$('input[name=end_date]').val(result[4]);
				$('input[name=location]').val(result[5]);
				$('input[name=duration]').val(result[6]);
				$('#addNewMainTrainingModal').modal('show');
				document.location.hash = "editTraining2-" + trainingId;
			});
			
			//trainingId pole olemas - ehk olukord 2, kui hakatakse looma uut koolitust
		} else {
			$('input[name=id]').val(0);
			$('input[name=name]').val('');
			$('input[name=start_date]').val('');
			$('input[name=end_date]').val('');
			$('input[name=lecturer]').val('');
			$('input[name=location]').val('');
			$('input[name=duration]').val('');
			$('#addNewMainTrainingModal').modal('show');
			document.location.hash = "addTraining2";
		}
	},
	
	remove: function(trainingId) {
		var confirm = window.confirm("Kustuta koolitus?");
		if(confirm) {
			$.ajax({
				method: "POST",
			  	url: "src/php/contents/trainings2.php?action=deleteById",
			  	data: { id: trainingId }
			})
			.done(function() {
				window.location = 'main.php?view=trainings2';
			});
		}
	},
	
	viewParticipants: function(trainingId) {
		$.ajax({
			method: "POST",
			//url määrab, kuhu postitakse
		  	url: "src/php/contents/users.php?action=getTraining2Users",
		  	// data näitab, mida postitakse ehk postitakse (json tüüpi) objekt mille üks väli id
		  	data: { trainingId: trainingId }
		})
		// done garanteerib, et seda tegevust siin tehakse alles siis, kui ajax on tehtud
		// result on massiiv, mis echotakse ja kus on ühe andmerea andmed massiivina
		.done(function(result) {
			var dropdownUsers = result.not_registered;
			var registeredUsers = result.registered;
			
			// draw dropdown
			var select = $('#userSelect');
			$(select).empty();
			$('<option>').val(0).text("--- Valid kasutaja ---").appendTo(select);
			$.each(dropdownUsers, function(key, value) {              
		        $('<option>').val(value.id).text(value.firstname + " " + value.lastname).appendTo(select);     
		    });
			
			// draw table
			var table = $('#usersTable');
			var count = 0;
			$(table).empty();
			$.each(registeredUsers, function(key, value) {
				var row = '<tr>';
				row += '<td scope="row">' + value.id + '</td>';
				row += '<td>'+value.firstname + " " + value.lastname +'</td>';
				row += '<td><button type="button" class="btn btn-info btn-sm" onclick="Training2.removeParticipant('+value.id+','+trainingId+');">Eemalda</button></td>';
				row += '<td><button type="button" class="btn btn-info btn-sm" onclick="Certificate.pdf('+value.id+',' + trainingId+');">Väljasta tunnistus</button></td>';
				row += '</tr>';
		        $(table).append(row);
		        count++;
		    });
			
			$('#participants-'+trainingId).html(count);
			$('input[name=trainingId]').val(trainingId);
			$('#viewParticipants').modal('show');
		});
	},
	
	addParticipant: function() {
		var userId = $('#userSelect').val();
		var trainingId = $('input[name=trainingId]').val();
		
		$.ajax({
			method: "POST",
		  	url: "src/php/contents/trainings2.php?action=addParticipant",
		  	data: { trainingId: trainingId, userId: userId }
		})
		.done(function(result) {
			console.log("testing, added?");
			// TODO: refresh page
			Training2.viewParticipants(trainingId);
		});
	},
	
	removeParticipant: function(userId, trainingId) {		
		$.ajax({
			method: "POST",
		  	url: "src/php/contents/trainings2.php?action=removeParticipant",
		  	data: { trainingId: trainingId, userId: userId }
		})
		.done(function(result) {
			console.log("testing, removed?");
			//TOOD: refresh page
			Training2.viewParticipants(trainingId);
		});
	}
};

$(document).ready(function(){
	console.log("loaded, check hash");
	History.checkHash();
	setInterval(History.checkHash, 1000);
	
	$('#addNewMainTrainingModal').on('hidden.bs.modal', function () {
		console.log("ss");
		document.location.hash = '';
	});
});
