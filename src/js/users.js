jQuery( function(){

	function users_info_longpolling( timestamp, lastUserId ){
		var t;

		if( typeof lastUserId == 'undefined' ){
			lastUserId = 0;
		}
				
		jQuery.ajax({
			url: 'src/php/get_users_info.php',
			type: 'GET',
			data: 'timestamp=' + timestamp + '&lastUserId=' + lastUserId,
			dataType: 'json',
			success: function( data ){
				clearInterval( t );
				if( data.status == 'results' || data.status == 'no-results' ){
					t=setTimeout( function(){
						users_info_longpolling( data.timestamp, data.lastUserId );
					}, 1000 );
					if( data.status == 'results' ){
						jQuery.each( data.users_info, function(i,user){
							if( jQuery('.no-items').size() == 1 ){
								jQuery('.items').empty();
							}
							if( jQuery('#' + user.id).size() == 0 ){
								jQuery('.items').prepend( '<tr><td>' + user.id + '</td><td>' + user.role + '</td><td>' + user.gender + '</td><td>' + user.firstname + '</td><td>' + user.lastname + '</td><td>' + user.username + '</td><td>' + user.ss_number + '</td><td>' + user.address + '</td><td>' + user.phone + '</td><td>' + user.school + '</td><td>' + user.supervisor_name + '</td><td>' + user.parent_name + '</td><td>' + user.parent_email + '</td><td>' + user.parent_phone + '</td></tr>' );
							}
						});
					}
				} else if( data.status == 'error' ){
					alert('Error! Please refresh the page!');
				}
			},
			error: function(){
				clearInterval( t );
				t=setTimeout( function(){
					users_info_longpolling( data.timestamp, data.lastUserId);
				}, 15000 );
			}
		});
	}
	users_info_longpolling( new Date().getTime() / 1000 | 0 );
});