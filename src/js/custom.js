/*(function ($) {

    $.address.change(function(event) { // event.value = /home
	$.get('src/php/contents/' + event.value + '.php', function(data) {
		$('#contents_div').html(data);
	});
});
    
})(jQuery);*/

/*$(document).ready(function() {
    // INITIAL CONTENT ON FIRST LOAD
$("#contents_div").load('./src/php/contents/home.php');

// CODE FROM HISTORY.JS 
(function(window, undefined) {
var History = window.History; 
if (!History.enabled) {
    return false;
}

History.Adapter.bind(window, 'statechange', function() { 
    var State = History.getState(); 
    History.log(State.data, State.title, State.url);
    });

    // EVENT LISTENER FOR NAVBUTTON CLASS TO REPLACE CONTENT IN MAINCONTENT DIV
$('.navButton').click(function(e) {
    e.preventDefault();
    pageurl = $(this).attr('name');
    $("#mainContent").fadeOut().load($(this).attr("href")).fadeIn();
    if (pageurl != window.location) {
        history.pushState('', $(this).attr('title'), $(this).attr('name'));
        }
    });

})(window);
});*/

$(function() {
  String.prototype.decodeHTML = function() {
    return $("<div>", {html: "" + this}).html();
  };

  var $main = $("main"),
  
  init = function() {
    $("#main").load("src/php/contents/home.php");
  },
  
  ajaxLoad = function(html) {
    document.title = html
      .match(/<title>(.*?)<\/title>/)[1]
      .trim()
      .decodeHTML();

    init();
  },
  
  loadPage = function(href) {
    //$main.load(href + " main>*");
	console.log(href);
	$("main").load(href);
  };
  
  init();
  
  $(window).on("popstate", function(e) {
    if (e.originalEvent.state !== null) {
      loadPage(location.href);
    }
  });

  $(document).on("click", "a, area", function() {
    var href = $(this).attr("href");

    if (href.indexOf(document.domain) > -1
      || href.indexOf(':') === -1)
    {
      history.pushState({}, '', href);
      loadPage(href);
      return false;
    }
  });
});

/*$(function(){
	$("a[rel='tab']").click(function(e){
		//e.preventDefault(); 
			
		if uncomment the above line, html5 nonsupported browers won't change the url but will display the ajax content;
		if commented, html5 nonsupported browers will reload the page to the specified link. 
		
		
		//get the link location that was clicked
		pageurl = $(this).attr('href');
		
		//to get the ajax content and display in div with id 'content'
		$.ajax({url:pageurl+'?rel=tab',success: function(data){
			$('main').html(data);
		}});
		
		//to change the browser URL to 'pageurl'
		if(pageurl!=window.location){
			window.history.pushState({path:pageurl},'',pageurl);	
		}
		return false;  
	});
});

 the below code is to override back button to get the ajax content without reload
$(window).bind('popstate', function() {
	$.ajax({url:location.pathname+'?rel=tab',success: function(data){
		$('main').html(data);
	}});
});*/