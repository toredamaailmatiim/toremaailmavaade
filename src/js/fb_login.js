function getQueryParams(qs) {
    qs = qs.split("+").join(" ");

    var params = {}, tokens,
        re = /[?&]?([^=]+)=([^&]*)/g;

    while (tokens = re.exec(qs)) {
        params[decodeURIComponent(tokens[1])]
            = decodeURIComponent(tokens[2]);
    }

    return params;
}

var query = getQueryParams(document.location.search);

var redirect = "";

if(query.redirect) {
	console.log(query.redirect);
	redirect = "&redirect="+query.redirect;
};
	
window.fbAsyncInit = function() {
	FB.init({
	appId      : '448803805269089',
	status     : true, 
	cookie     : true, 
	xfbml      : true  
	});
};
		       
(function(d){
	var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
	if (d.getElementById(id)) {return;}
	js = d.createElement('script'); js.id = id; js.async = true;
	js.src = "https://connect.facebook.net/en_US/all.js";
	ref.parentNode.insertBefore(js, ref);
}(document));

function FBLogin(){
	FB.login(function(response){
		if(response.authResponse){
			window.location.href = "src/php/login.php?action=FB_logIN"+redirect;
		}
	}, {scope: 'email'});
	
}