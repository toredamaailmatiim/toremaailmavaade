-- MySQL dump 10.13  Distrib 5.6.22, for Linux (x86_64)
--
-- Host: localhost    Database: tarretis_toremaailmavaade
-- ------------------------------------------------------
-- Server version	5.6.22-cll-lve

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tokens`
--

DROP TABLE IF EXISTS `tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tokens` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `used` int(11)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tokens`
--

LOCK TABLES `tokens` WRITE;
/*!40000 ALTER TABLE `tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trainings`
--

DROP TABLE IF EXISTS `trainings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trainings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(225) NOT NULL,
  `date` date NOT NULL,
  `lecturer` varchar(25) NOT NULL,
  `location` varchar(25) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trainings`
--

LOCK TABLES `trainings` WRITE;
/*!40000 ALTER TABLE `trainings` DISABLE KEYS */;
/*!40000 ALTER TABLE `trainings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trainings_lecturers`
--

DROP TABLE IF EXISTS `trainings_lecturers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trainings_lecturers` (
  `trainings_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trainings_lecturers`
--

LOCK TABLES `trainings_lecturers` WRITE;
/*!40000 ALTER TABLE `trainings_lecturers` DISABLE KEYS */;
/*!40000 ALTER TABLE `trainings_lecturers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trainings_participants`
--

DROP TABLE IF EXISTS `trainings_participants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trainings_participants` (
  `trainings_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trainings_participants`
--

LOCK TABLES `trainings_participants` WRITE;
/*!40000 ALTER TABLE `trainings_participants` DISABLE KEYS */;
/*!40000 ALTER TABLE `trainings_participants` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `ss_number` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `role` tinyint(4) NOT NULL,
  `registered` datetime NOT NULL,
  `last_login` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `username`, `password`, `ss_number`, `firstname`, `lastname`, `gender`, `address`, `phone`, `active`, `role`, `registered`, `last_login`) VALUES (1,'admin@test.ee','21232f297a57a5a743894a0e4a801fc3','12345678910','Test','User','male','','',1,1,'2015-03-01 21:45:00','0000-00-00 00:00:00'),(2,'test@hot.ee','098f6bcd4621d373cade4e832627b4f6','test','test','test','0','test','test',1,1,'2015-03-01 22:41:33','0000-00-00 00:00:00'),(3,'test2@hot.ee','098f6bcd4621d373cade4e832627b4f6','test','test','test','0','test','test',1,1,'2015-03-01 22:44:55','0000-00-00 00:00:00'),(4,'umarzarip@hotmail.co.uk','d6fc5b16681fa2d2b298138940403975','30301190290','Umar','Zarip','0','narva 89 320','56603512',1,1,'2015-03-08 13:44:47','0000-00-00 00:00:00'),(5,'pakisupp@gmail.com','fc0c0b80155ecd74bd232ce7bd50dc08','39002132716','aivo','toots','0','test','',1,1,'2015-03-08 15:09:40','0000-00-00 00:00:00'),(6,'sdgsdgsdg@hot.ee','25f9e794323b453885f5181f1b624d0b','gsdsdgsdg','sdfhsdg','gdgsdsdgdg','0','sdgsdgdg','dssdgsdgsdg',1,1,'2015-03-08 23:47:44','0000-00-00 00:00:00'),(7,'sellinmargus@gmail.com','','','Margus','Sellin','male','','',0,0,'2015-03-09 07:57:21','2015-03-09 07:57:21'),(8,'kaisellin@gmail.com','','','Kai','Sellin','female','','',0,0,'2015-03-09 07:58:04','2015-03-09 07:58:04'),(9,'pakisupp@msn.com','','','Aivo','Toots','male','','',0,0,'2015-03-09 09:28:21','2015-03-09 09:28:21'),(10,'test2@gmail.com','69fb46f4c18463dd25002aeffc0257d1','12345678','Kai','Sellin','0','kase 5','',1,1,'2015-03-09 10:47:21','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'tarretis_toremaailmavaade'
--

--
-- Dumping routines for database 'tarretis_toremaailmavaade'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-03-22 17:11:30
