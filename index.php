<?php

session_start();

// Kui kasutaja on sattunud antud lehele sisse-logimata, suuname ta tagasi avalehele

if (isset($_SESSION['username'])) {

	header ( 'Location: main.php' );

}

$redirect = filter_input(INPUT_GET, 'redirect', FILTER_SANITIZE_STRING);

?>

<!DOCTYPE html>
<html lang="ee">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Aivo Toots, Kai Sellin, Umar Zarip">

    <title>TORE Maailmavaade</title>
    
    <link rel="icon" href="hpclub.pri.e/templates/HPclub/images/favicon.ico" />

    <!-- Bootstrap core CSS -->
    <link href="src/css/bootstrap.min.css" rel="stylesheet">
    
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    

    <!-- Facebook Login JavaScript -->
    <script src="src/js/fb_login.js"></script>
    
    <script src="src/js/tore.js"></script>
  </head>

  <body>

    <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">TORE</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">Esileht</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <div class="container">
        <div class="row">&nbsp;</div>
        <div class="row">&nbsp;</div>
        <div class="row">&nbsp;</div>
        <div class="row">&nbsp;</div>
        <div class="row">
            <div class="col-md-9">
        
      <!-- Main component for a primary marketing message or call to action -->
      <div class="jumbotron">
        <h1>Tere tulemast!</h1>
        <p>Siin on TORE Maailmavaade tiimi projekti testkeskkond.</p>
        <p></p>
        <p>Tiimi liikmed: Aivo Toots, Umar Zarip ja Kai Sellin</p>
      </div>
        </div>
        <div class="col-md-3 blog-sidebar">
        <div class="sidebar-module">
          <fieldset>
          <form class="form-signin" action="src/php/login.php?action=logIN<?php if ( isset($redirect) && $redirect != "") { echo "&amp;redirect=".$redirect; } ?>" method="post">
	        <h2 class="form-signin-heading text-center">Logi sisse</h2>
	        <label for="inputEmail" class="sr-only">E-maili aadress</label>
	        <input type="email" name="inputEmail" id="inputEmail" class="form-control" placeholder="E-maili aadress" required autofocus>
	        <label for="inputPassword" class="sr-only">Parool</label>
	        <input type="password" name="inputPassword" id="inputPassword" class="form-control" placeholder="Parool" required>
	        <a class="pull-right" href="reset.html">Unustasid parooli?</a>
	        <div class="checkbox" style="width:150px;">
	          <label>
	            <input type="checkbox" value="remember-me"> Jäta mind meelde
	          </label>
	        </div>
	        <input class="btn btn-primary btn-block" type="submit" value="Logi sisse">
	        <br>
        </form>
        <form>
        <input class="btn btn-primary btn-block" type="button" onclick="FBLogin()" value="Logi sisse Facebook'ga">
        <p></p>
        <p class="text-center"><a href="register.html">Registreeri</a></p>
        </form>
        </fieldset>
      
        </div><!-- /.blog-sidebar -->
        </div>
        </div>
    </div> <!-- /container -->
    
  </body>
</html>
